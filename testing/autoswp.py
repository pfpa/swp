#!/usr/bin/env python3
"Zur Automatizieren von SWP tests"
# Nutzung: ./autoswp.py [schueler] [kurse]
# Author: Philip Kaludercic, Q11 <philippija@gmail.com>
# License: MIT (see http://phikal.mit-license.org)

import sys
from random import randint as rand, choice as ch
from time import sleep

import requests as req

if len(sys.argv) < 3:
    print('No files specified', file=sys.stderr)
    exit()

def nametouser(line): # multiples ignored
    "Generiert Benutzernamen aus einer .csv Zeile"
    first, last, _, _ = line.split(';')
    return (first[0:4]+last[0:4]).lower()

EINZEL = [str(x) for x in [0, 1, 2, 3]]
MANNSC = [str(x) for x in [4, 5, 6, 7]]
ZUSATZ = [str(x) for x in [8, 9]]
ALLESP = EINZEL + MANNSC + ZUSATZ

try:
    config = req.post("http://localhost:4000/actio/starte", files={
        "schueler": open(sys.argv[1], 'rb'),
        "kurse": open(sys.argv[2], 'rb')
    }, data={"max": '5'}, auth=('', ''))
    print("Server konfiguriert... {}".format(config.text))

    with open(sys.argv[1]) as f:
        for u in [nametouser(l) for l in f]:
            if rand(0, 10) < 2:
                continue # non-voter, 30%

            data = {
                'user' : u,
                'pass' : '',
                'geschl' : 'w',
                'ausschluss' : '-1'
            }

            if rand(0,1) < 1:
                data['geschl'] = 'm'

            if rand(0, 10) < 3: # additum (20% wsk fuer additum)
                data['additum'] = 'x'
                data['einzel_adit'] = ch(EINZEL)
                data['manschaft_adit'] = ch(MANNSC)
            else:
                e = data['einzel'] = ch(EINZEL)
                m = data['manschaft'] = ch(MANNSC)
                s1 = data['sport1'] = ch([x for x in ALLESP if x not in [e]])
                #s1 = data['sport1'] = 0
                s2 = data['sport2'] = ch([x for x in ALLESP if x not in [m]])
                e1 = data['ersatz1'] = ch([x for x in ALLESP if x not in [e, m, s1, s2]])
                #e1 = data['ersatz1'] = 0
                e2 = data['ersatz2'] = ch([x for x in ALLESP if x not in [e, m, s1, s2, e1]])
                #e2 = data['ersatz2'] = 0
                if rand(0, 10) < 5: # 50% ausschluss wsk
                    data['ausschluss'] = ch([x for x in ALLESP if x not in [e, m, s1, s2, e1, e2]])

            res = req.post('http://localhost:8080/submit', data=data)
            print('Voted for {}:\t{}'.format(u, res.text))
            print(data)
            sleep(0)

    beende = req.post("http://localhost:4000/actio/beende", auth=('', ''))
    print("Server beendet... {}".format(beende.text))
except req.exceptions.ConnectionError:
    print("Couldn't connect...", file=sys.stderr)
except KeyboardInterrupt:
    pass
