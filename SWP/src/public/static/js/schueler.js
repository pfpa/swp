
// SCHUELER.js - fuer die interaktive schueler benutzeroberflaeche - SWP 2016

function istzuoft() {
        if ($('input[name=additum]').is(':checked'))
                return false;

        var felder = [
                "einzel",
                "manschaft",
                "sport1",
                "sport2",
                "ersatz1",
                "ersatz2"
        ], wahl = {};

        for (var f in felder) {
                wahl[felder[f]] = $('select[name='+felder[f]+']').val();
        }

        var c = {};
        for (var s in wahl) {
                if (wahl[s] == -1) continue;
                if (!c[wahl[s]]) c[wahl[s]] = 1;
                else c[wahl[s]]++;

                if (c[wahl[s]] > 2)
                        return true
        }
        return false
}

function wahlbereit() {
        return !(($.trim($('input[name=user]').val())) &&
                        ($.trim($('input[name=pass]').val())) &&
                        ($('input#akz').is(':checked')) &&
                        ($('input[name=additum]').is(':checked') ? (
                                                                    ($('select[name=einzel_adit]').val() != -1) &&
                                                                    ($('select[name=manschaft_adit]').val() != -1)	     
                                                                   ) : (
                                                                           ($('select[name=einzel]').val() != -1) &&
                                                                           ($('select[name=manschaft]').val() != -1) &&
                                                                           ($('select[name=sport1]').val() != -1) &&
                                                                           ($('select[name=sport2]').val() != -1) &&
                                                                           ($('select[name=ersatz1]').val() != -1) &&
                                                                           ($('select[name=ersatz2]').val() != -1)
                                                                       ))) || istzuoft();
}

function optionenLaden() {
        var name = $('input[name=user]').val(),
        status = $('#status'),
        focus = function(pass) {
                if (pass)
                        $('table select').empty();
                $('select').prop('disabled', pass);
        }

        $.getJSON('/api/wahl?name='+name, function (data) {
                if (jQuery.isEmptyObject(data)) {
                        status.show();
                        status.text('Geben sie einen validen Benutzernamen ein')
                                focus(true);
                } else for (var key in data) {
                        status.hide();
                        focus(false);
                        $('select[name='+key+']').empty();
                        for (var spart in data[key])
                                jQuery('<option/>', {
                                        value: data[key][spart],
                                        text: spart
                                }).appendTo('select[name='+key+']');
                }
        }).fail(function() {
                focus(true);
        });
}

$(function () {
        $($('input[name=additum]').is(':checked') ? '#ifNo' : '#ifYes').hide();
        $('select').val(-1);

        var submit = $('input[type=submit]'),
        status = $('#status'),
        update = function (wahl) {
                submit.prop('disabled', wahlbereit());
        };

        status.hide();
        $('input[name=additum]').click(function () {
                if ($('input[name=additum]').is(':checked')) {
                        $('#ifYes').show();			
                        $('#ifNo').hide();
                } else {
                        $('#ifYes').hide();
                        $('#ifNo').show();
                }
                $('select').val(-1);
                update();
                optionenLaden();
        });

        $('input[name=user]').bind('input', optionenLaden);
        $('input#akz').click(update);
        $('input[name=additum]').click(update);
        $('input[name=user]').bind('input', update);
        $('input[name=pass]').bind('input', update);
        $('table#ifYes select').change(update);
        update();

        $('table#ifNo select').change(function () {
                var section = $(this).attr('name'),
                ID = $(this).children(':selected').val();

                var wahl = {};

                if (section == 'ausschluss') {
                        $('select[name!=ausschluss]').filter(function() {
                                if ($(this).val() === ID) {
                                        delete wahl[$(this).attr('name')];
                                        return true;
                                }
                                return false;
                        }).val(-1);
                        $('option').show();
                        $('option[value='+ID+']').hide();
                }

                status.hide();
                if (istzuoft(wahl))
                        status.text('Sie konnen eine Sportart nur 2 mal auswahlen.').show();

                update();
        });
        optionenLaden();
});
