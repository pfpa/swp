
// ADMIN.js - fuer die interaktive leher benutzeroberflaeche - SWP 2016

function getstatus(cb) {   
    $.get('/api/staus', function (res) {
	if (cb) cb(res);
    });
}

function beendec(res) {
    $('input[id=bee]').prop('disabled', !(res == 2 && $('#beendec').attr("checked")));
}

function updatestatus(res) {
    var text = 'FEHLER ('+res+')', color = '#000', bg = '#e33';
   
    var starten = $('input[id=sta]'),
	beenden = $('input[id=bee]'),
	anzeige = $('input[id=anz]');
    
    starten.prop('disabled', true);
    beenden.prop('disabled', true);
    anzeige.prop('disabled', true);
    
    if (res == 0) { 
	text = 'NOCH NICHT ANGEF.';
	color = '#fff';
	bg = '#000';
	starten.prop('disabled', false);
    } else if (res == 2) {
	text = 'IN DER WAHL';
	color = '#fff';
	bg = '#33e';
	beendec(res);
    } else if (res == 3) {
	text = 'IN DER OPTIMZ.';
	bg = '#3ee';
    } else if (res == 4) {
	text = 'FERTIG';
	bg = '#3e3';
	anzeige.prop('disabled', false);
    } // an sonsten, fehler (siehe default Werte)
    
    $('#status').text(text)
	.css('color', color)
	.css('background', bg)
	.css('border-width', '0'); // wegen .css
}

$(function () {
    $('#js_warn').hide();

    $("#beendec").change(function () {
	getstatus(beendec);
    });
    
    $('#update').click(function () {
	getstatus(updatestatus);
	if ($('#zeigelist').attr("checked")) {
	    $.get('/api/liste'+($('#nichtwahl').attr("checked") ? "?nichtwahl=1" : ""), function (data) {
		$('#stable').text('');
		$('#stable').append("<tr><th>Name</th><th>Vorname</th><th>Klasse</th><th>Hat Gewahlt?</th><tr>");
		if (data) {
		    var lines = data.split('\n');
		    for (var i = 0; i < lines.length; i++) {
			var parts = lines[i].split(',');
			$('#stable').append("<tr>");
			for (var j = 0; j < parts.length; j++)
			    $('#stable').append("<td>"+parts[j]+"</td>");
			$('#stable').append("</tr>");
		    }
		}
		$('stwrap').show();
	    });
	} else $('#stable').text("Nicht geladen");
    });
    
    getstatus(updatestatus);
});
