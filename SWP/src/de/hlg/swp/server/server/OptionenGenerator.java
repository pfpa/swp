package de.hlg.swp.server.server;

import com.eclipsesource.json.JsonObject;
import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Sportkurs;
import de.hlg.swp.server.Router;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static de.hlg.swp.Main.adm;

/**
 * Diese Klasse generiert die Kurse die ein Schueler waehlen kann.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class OptionenGenerator implements Router.Response {
    /**
     * Map die die moeglichen Optionstypen bereitstellt
     */
    final private static Map<String, Integer> opts = new HashMap<>();
    /**
     * Enthaelt die erlaubten Sportarten fuer den abgefragten Benutzer.
     */
    final private static Map<String, JsonObject> cache = new ConcurrentHashMap<>();

    /** Initalisiert {@link OptionenGenerator#opts} */
    static {
        opts.put("einzel", Sportkurs.EINZEL);
        opts.put("manschaft", Sportkurs.MANNSCHAFT);
        opts.put("sport1", Sportkurs.ALLE);
        opts.put("sport2", Sportkurs.ALLE);
        opts.put("ersatz1", Sportkurs.MANNSCHAFT | Sportkurs.EINZEL);
        opts.put("ersatz2", Sportkurs.MANNSCHAFT | Sportkurs.EINZEL);
        opts.put("ausschluss", Sportkurs.ALLE);

        opts.put("einzel_adit", Sportkurs.EINZEL);
        opts.put("manschaft_adit", Sportkurs.MANNSCHAFT);
    }

    /**
     * Gibt die moeglichen Kurse die der Schueler waehlen kann zurueck.
     *
     * @param session Informationen die der Server fuer die Methode zur Verfuegung stellt.
     */

    public Router.Data reply(IHTTPSession session) {
        JsonObject obj = new JsonObject();

        Map<String, String> params = session.getParms();
        Schueler sch;
        if (!params.containsKey("name")) return null;
        String name = params.get("name");

        if (cache.containsKey(name))
            obj = cache.get(name);
        else {
            sch = adm.gebeDaten().UserToSchueler(name);
            if (sch != null) for (String sportart : opts.keySet()) {
                JsonObject optionen = new JsonObject()
                        .add("[Bitte auswahlen]", -1);
                adm.gebeDaten().gebeKurse().stream()
                        .filter(k -> k.istTyp(opts.get(sportart)))
                        .filter(k -> !(k.istNurMaennlich() && sch.istWeiblich()))
                        .filter(k -> !(k.istNurWeiblich() && sch.istMannlich()))
                        .sorted(Comparator.comparing(Sportkurs::gebeName))
                        .forEach(k -> optionen.add(k.gebeName(), k.gebeID()));
                obj.add(sportart, optionen);
            }
            cache.put(name, obj);
        }

        Router.Data d = new Router.Data();
        d.mime("application/json");
        d.body(obj.toString());
        return d;
    }
}
