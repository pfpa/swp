package de.hlg.swp.server.server;

import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Wunsch;
import de.hlg.swp.server.Router;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response.Status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static de.hlg.swp.Main.adm;

/**
 * Annehmen, prüfen und bei Erfolg anschliessendes speichern einer Wahl.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Wahlverarbeiter implements Router.Response {

    /**
     * Hier wird ein Schueler verarbeitet. Die Daten des Servers werden in der Methode in einer {@code
     * Map<String,String>} zwischengespeichert.
     *
     * @param session Informationen die der Server fuer die Methode zur Verfuegung stellt.
     */

    public Router.Data reply(IHTTPSession session) {
        Router.Data d = new Router.Data();
        Map<String, String> params = session.getParms();
        try {
            session.parseBody(params);
        } catch (Exception e) {
            d.error(e.getMessage());
            d.status(Status.BAD_REQUEST);
            return d;
        }

        String username = params.get("user").trim();
        String pass = params.get("pass").trim();
        Schueler sch;
        try {
            sch = adm.gebeDaten().UserToSchueler(username);
        } catch (NullPointerException npe) { // wenn benuzername nicht gefufunden
            d.status(Status.UNAUTHORIZED);
            d.error("Benutzername nicht gefunden");
            return d;
        }

        //Integritaet der Daten ueberpruefen
        if (!sch.passOK(pass)) {// wenn passwort falsch
            d.error("Falsches Passwort eingegeben");
            d.status(Status.UNAUTHORIZED);
            return d;
        }

        // Hier soll Ueberprueft werden ob alle Variablennamen
        // existieren
        if (params.containsKey("additum") && !params.get("additum").isEmpty()) {
            if (!(params.containsKey("einzel_adit") && params.containsKey("manschaft_adit"))) {
                d.error("Nicht alle Daten sind korrekt uebermittelt worden!");
                d.status(Status.NOT_FOUND);
                return d;
            }
        } else {
            if (!(params.containsKey("einzel") && params.containsKey("manschaft")
                    && params.containsKey("sport1") && params.containsKey("sport2")
                    && params.containsKey("ersatz1") && params.containsKey("ersatz2"))) {
                d.error("Nicht alle Daten sind korrekt uebermittelt worden!");
                d.status(Status.BAD_REQUEST);
                return d;
            }
        }

        try {
            boolean additum = params.containsKey("additum") && !params.get("additum").isEmpty();
            List<Wunsch> wunsche = new ArrayList<>(additum ? Arrays.asList(
                    new Wunsch(params.get("einzel_adit"), Wunsch.ERSTWAHL),
                    new Wunsch(params.get("manschaft_adit"), Wunsch.ERSTWAHL)
            ) : Arrays.asList(
                    new Wunsch(params.get("einzel"), Wunsch.ERSTWAHL),
                    new Wunsch(params.get("manschaft"), Wunsch.ERSTWAHL),
                    new Wunsch(params.get("sport1"), Wunsch.ERSTWAHL),
                    new Wunsch(params.get("sport2"), Wunsch.ERSTWAHL),
                    new Wunsch(params.get("ersatz1"), Wunsch.ALTWAHL),
                    new Wunsch(params.get("ersatz2"), Wunsch.ALTWAHL)
            ));

            if (params.containsKey("ausschluss") && !params.get("ausschluss").equals("-1"))
                wunsche.add(new Wunsch(params.get("ausschluss"), Wunsch.NICHTWAHL));

            if (sch.updateInfo(wunsche, additum)) {
                d.error("Die abgegebenen Daten sind fehlerhaft");
                d.status(Status.NOT_ACCEPTABLE);
                return d;
            }
        } catch (NumberFormatException nfe) {
            nfe.getLocalizedMessage();
            d.status(Status.NOT_ACCEPTABLE);
            d.error("Invalide ID: " + nfe.getLocalizedMessage());
            return d;
        }

        // Neuerungen sichern
        d.status(Status.ACCEPTED);
        d.put("Refresh", "3; url=/");
        d.body("Wurde vernommen.");
        return d;
    }
}
