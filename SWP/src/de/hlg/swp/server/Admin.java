package de.hlg.swp.server;

import de.hlg.swp.models.Nutzer;
import de.hlg.swp.server.admin.*;
import de.hlg.swp.server.admin.Terminator;
import de.hlg.swp.utils.Backup;
import de.hlg.swp.utils.Daten;
import de.hlg.swp.utils.Optimierer;

import java.util.Random;

import static de.hlg.swp.Main.*;
import static fi.iki.elonen.NanoHTTPD.Method.*;
import static fi.iki.elonen.NanoHTTPD.Method.POST;

/**
 * Dies ist die Klasse zum steuern der Admin-Oberflaeche.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Admin {
    /** Statische Konstante zum anzeigen des Serverstatuses */
    public static final int SERVER_FEHLER = -1,
            NOCH_NICHT_ANGEF = 0,
            /* ehemalig: LISTE_ANZEIGEN = 1, */
            IN_DER_WAHL = 2,
            IN_DER_OPTIMIZ = 3,
            FERTIG = 4;
    /** Variable zum anzeigen des Serverstatuses */
    private int status = NOCH_NICHT_ANGEF;
    /** Dieses Objekt enthaelt den Admin-Server. Er dient zur Steuerung der Benutzeroberflaeche. */
    private volatile Router server;
    /** Dieses Objekt enthaelt den Schuelerserver. Auf diesem findet die Wahl der Schueler statt. */
    private volatile Server schuelerServer;
    /** Epoch-zeit seit Server start */
    private long umfrageStart;
    /** Dieses Objekt wird benoetigt um sich als Admin anzumelden */
    private Nutzer adm;
    /** Dieses Objekt enhaelt Optimierer, welches fuer die Verteilung der Schueler auf die Kurse zustaendig ist. */
    private Optimierer optimierer;
    /** Dieses Objekt enthaelt die alle Runtime Daten (Kurse, Schueler, Wahlen, ...). */
    private Daten daten;

    /** Konstruktor welcher einen Server mit startet. Aufrufen der Methode {@link Admin#server()} */

    public Admin() {
        server();
    }

    /**
     * Konstruktor mit Option der Statuswahl. Aufrufen der Methode {@link Admin#server()}
     * <p>
     * Startet den Wahlserver wenn regenerierter status IN_DER_WAHL ist
     *
     * @param bcp Backup, welches den Server initiert.
     */

    public Admin(Backup bcp) {
        if ((this.setzeStatus(bcp.status)) == IN_DER_WAHL)
            setzeSchuelerServer(new Server());
        daten = new Daten(bcp.schueler, bcp.kurse);
        server();
    }

    /**
     * Getter fuer {@link Admin#daten}
     *
     * @return {@code Daten} gibt das Datenobjekt des Servers.
     */

    public Daten gebeDaten() {
        return daten;
    }

    /**
     * Setter fuer {@link Admin#daten}
     *
     * @param daten Muss vom Typ {@link de.hlg.swp.utils.Daten} sein.
     */

    public void setzeDaten(Daten daten) {
        this.daten = daten;
    }

    /**
     * Getter fuer {@link Admin#status}
     *
     * @return int {@link Admin#status}
     */

    public int getStatus() {
        return status;
    }

    /**
     * <p>Methode die den Server startet. Der Port des Servers ist zwischen 2^10 und 2^16.</p> <p>Ausnahme: Wenn das
     * Programm im Debug-Modus laeuft ist der Port festegelet auf 4000. </p> Die einzelnen Seiten die zur verfuegung
     * stehen sind: <ul> <li>actio/start</li> <li>actio/beende</li> <li>actio/backup</li> <li>api/liste</li>
     * <li>output</li> <li>schuelerlist</li> </ul> Aufbau und verhalten siehe {@link de.hlg.swp.server.admin}. Der
     * Server verwendet ein Router objekt zum routen request.
     */

    private void server() {
        int port = 10240 + (new Random().nextInt((1 << 16) - 10240)) - 1;
        if (conf.DEBUG) port = 4000; // debug port

        setzeAdmin(new Nutzer());

        server = new Router(port, "adm");
        try {
            // Statische Datein
            String DIR = "/public/static";
            server.addRouteStatic(GET, "/", DIR + "/Lehreroverlay.html", "text/html");
            server.addRouteStatic(GET, "/js/jquery.js", DIR + "/js/jquery.js", "text/html");
            server.addRouteStatic(GET, "/js/admin.js", DIR + "/js/admin.js", "application/x-javascript");
            server.addRouteStatic(GET, "/css/style.css", DIR + "/css/style.css", "text/css");

            // Bilder
            server.addRouteStatic(GET, "/img/logo.jpg", DIR + "/img/logo.jpg", "image/jpeg");
            server.addRouteStatic(GET, "/img/sportarten.svg", DIR + "/img/sportarten.svg", "image/svg+xml");

            server.addRoute(POST, "/actio/starte", new Initialisator());
            server.addRoute(POST, "/actio/beende", new Terminator());
            server.addRoute(POST, "/actio/backup", new Backuper());
            server.addRoute(GET, "/wuensche", new WunschAnzeiger());
            server.addRoute(GET, "/output", new HTMLGenerator());
            server.addRoute(GET, "/schuelerlist", new SchuelerListGenerator());
            server.addRoute(GET, "/api/liste", new ListGenerator());
            server.addRoute(GET, "/csv", new CSVGenerator());

            server.addRoute(GET, "/api/staus", (session) -> {
                Router.Data d = new Router.Data();
                d.body(String.valueOf(status));
                return d;
            });

            server.start();
            log.info("Adminserver erfolgreich auf http://[ip]:" + port + "/ gestarted");
            log.info("Admin passwort: " + adm.gebePasswd());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Setter fuer {@link Admin#status}
     *
     * @param status <p>Muss einer der folgenden Werte haben</p> <ul> <li>{@link Admin#FERTIG}</li> <li>{@link
     *               Admin#IN_DER_OPTIMIZ}</li> <li>{@link Admin#IN_DER_WAHL}</li>
     *               <li>{@link Admin#NOCH_NICHT_ANGEF}</li> <li>{@link Admin#SERVER_FEHLER}</li> </ul>
     * @return int {@link Admin#server}
     */

    public int setzeStatus(int status) {
        this.status = status;
        return status;
    }

    /**
     * Getter fuer {@link Admin#adm}
     *
     * @return Nutzer ist ein normaler Benutzer, welcher alledings nur fuer die Leherer GUI zustaendig ist. Er kann
     * nicht waehlen.
     */

    public Nutzer gebeAdmin() {
        return adm;
    }

    /**
     * Setter fuer {@link Admin#adm}
     *
     * @param adm Muss vom Typ {@link de.hlg.swp.models.Nutzer} sein.
     */

    private void setzeAdmin(Nutzer adm) {
        this.adm = adm;
    }

    /**
     * Getter fuer {@link Admin#optimierer}
     *
     * @return Optimierer ist das {@code optimierer}-Objekt. Davon sollte es nur eines zur Laufzeit geben
     */

    public Optimierer gebeOptimiz() {
        return optimierer;
    }

    /**
     * Setter fuer {@link Admin#optimierer}
     *
     * @param o Muss vom Typ {@link Optimierer} sein.
     */

    public void setzeOptimiz(Optimierer o) {
        optimierer = o;
    }

    /**
     * Getter fuer {@link Admin#umfrageStart}
     *
     * @return {@code long} ist die Zeit in Sekunden.
     */

    public long gebeUmfrageStart() {
        return umfrageStart;
    }

    /**
     * Setter fuer {@link Admin#umfrageStart}
     *
     * @param us Muss vom Typ {@code long} sein.
     */

    public void setzeUmfrageStart(long us) {
        umfrageStart = us;
    }

    /**
     * Getter fuer {@link Admin#schuelerServer}
     *
     * @return {@code Server} ist das Schuelerserverobjekt.
     */

    public Server gebeSchuelerServer() {
        return schuelerServer;
    }

    /**
     * Setter fuer {@link Admin#schuelerServer}
     *
     * @param schuelerServer Muss vom Typ {@link de.hlg.swp.server.Server} sein.
     */

    public void setzeSchuelerServer(Server schuelerServer) {
        this.schuelerServer = schuelerServer;
    }
}
