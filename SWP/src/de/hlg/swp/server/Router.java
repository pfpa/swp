package de.hlg.swp.server;

import fi.iki.elonen.NanoHTTPD;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import static fi.iki.elonen.NanoHTTPD.Response.Status.INTERNAL_ERROR;
import static fi.iki.elonen.NanoHTTPD.Response.Status.NOT_FOUND;

/**
 * Die Router Klasse implementiert das Servergeruest. Router basiert auf NanoHTTPD (<a
 * href="https://github.com/NanoHttpd">Github NanoHttpd Project</a>)
 *
 * @author Phillip Kaludercic
 * @version 1.0
 */

public class Router extends NanoHTTPD {
    /** Enthaelt die moeglichen Responses auf HTTP Verben und Routen */
    final private Map<Method, Map<String, Response>> routes = new ConcurrentHashMap<>();
    /** Logger fuer HTTP verbindungen */
    private Logger log;

    /**
     * Konstuktor welcher Router initalisiert.
     *
     * @param port Port auf dem der Server laufen soll.
     * @param name Name des Loggers der erstellt wird oder der weitergefuehrt wird (,wenn er schon existiert).
     */

    public Router(int port, String name) {
        super(port);
        if (name != null)
            log = Logger.getLogger(name);
    }

    /** Haelt den Server an. */

    void halt() {
        super.stop();
    }

    /** Startet den Server. Servertimout ist auf 5000 ms gesetzt. */

    @Override
    public void start() throws IOException {
        super.start(SOCKET_READ_TIMEOUT, false);
    }

    /**
     * Fuegt eine moegliche Verbindung hinzu.
     *
     * @param method GET oder POST als Zugriffsart auf den Server.
     * @param route  Name der Route
     * @param reply  Klasse o.ae. zum verarbeiten der Anfrage.
     */

    public void addRoute(Method method, String route, Response reply) {
        if (routes.containsKey(method))
            routes.get(method).put(route, reply);
        else {
            routes.put(method, new HashMap<>());
            addRoute(method, route, reply);
        }
    }

    /**
     * Wrapper um {@link Router#addRoute(Method method, String route, Response reply)}
     *
     * @param method GET oder POST als Zugriffsart auf den Server.
     * @param req    Name der Route
     * @param res    Ort der Statischen Datei
     * @param mime   Typ der statischen Datei
     */

    public void addRouteStatic(Method method, String req, String res, String mime) {
        addRoute(method, req, new StaticResponse(res, mime));
    }

    /**
     * Generiert ein NanoHTTPD.Response Objekt.
     *
     * @param session Formatiert die Informationen fuer NanoHTTPD
     */

    @Override
    public final fi.iki.elonen.NanoHTTPD.Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String path = session.getUri();

        if (log != null) log.fine(String.format("%s :: %s %s",
                session.getHeaders().getOrDefault("REMOTE_ADDR", "XXX.XXX.X.XXX"), method.toString(), path));

        Data data;
        try {
            Response r = routes.get(method).getOrDefault(path,
                    new ErrorResponse(NOT_FOUND, "Coundn't find item"));
            data = r.reply(session);
        } catch (Exception e) {
            data = new ErrorResponse(INTERNAL_ERROR, e.getLocalizedMessage()).reply(session);
            e.printStackTrace();
        }

        NanoHTTPD.Response resp;

        if (data == null)
            resp = newFixedLengthResponse("");
        else if (data.stream != null)
            resp = newChunkedResponse(data.status, data.mime, data.stream);
        else
            resp = newFixedLengthResponse(data.status, data.mime,
                    data.error == null ? data.body :
                            "<!doctype html><html><body><h1>" + data.status.getDescription() + "</h1><p>" + data.error + "</p></html>");

        if (data != null)
            data.forEach(resp::addHeader);
        resp.setGzipEncoding(true);
        return resp;
    }

    /**
     * Die Response Klasse implementiert die Rueckgabeaktion. Sie ist Teil von Router.
     *
     * @author Phillip Kaludercic
     * @version 1.0
     */

    public interface Response {

        /**
         * Muss implementiert werden als Methode die auf einen HTTp Request antwortet und ein Response Objekt
         * generiert.
         *
         * @param session Stellt die Informationen bereit.
         * @return Ein Data-Objekt welches die Antwort auf die Anfrage enthaelt.
         */

        Data reply(IHTTPSession session);
    }

    /**
     * Die Data Klasse implementiert das Antwortverhalten des Servers. Sie ist Teil von Router.
     *
     * @author Phillip Kaludercic
     * @version 1.0
     */

    public static class Data extends HashMap<String, String> {
        /** Gibt den HTTP-Code zurueck. */
        private NanoHTTPD.Response.Status status = NanoHTTPD.Response.Status.OK;
        /** Inhalt des HTTP-Requests. */
        private String body = "";
        /** Typ des HTTP-Requests */
        private String mime = "text/html; charset=utf-8";
        /** Fehler der zurueckgegeben wird, wenn Error ungleich {@code null}. */
        private String error = null;

        /** Stream der zurueckgegeben wird, wenn stream ungleich {@code null} */
        private InputStream stream = null;

        /**
         * Setter fuer {@link Router.Data#status}
         *
         * @param status Setzt den Wert auf den der uebergeben wurde.
         */

        public void status(NanoHTTPD.Response.Status status) {
            this.status = status;
        }

        /**
         * Sugar-Setter fuer {@link Router.Data#body}
         *
         * @param title Enthaelt den Titel der Seite.
         * @param body Setzt den Wert auf den der uebergeben wurde.
         */

        public void html(String title, String body) {
            mime("text/html; charset=utf-8");
            this.body = "<!doctype html><html><meta charset=\"UTF-8\" /><title>" + title +
                    "</title></head><body>" + body + "</body></html>";
        }

        /**
         * Sugar-Setter fuer {@link Router.Data#body}
         *
         * @param title Enthaelt den Titel der Seite.
         * @param body Setzt den Wert auf den der uebergeben wurde.
         * @param css Soll den Style-Teil der Seite enthalten.
         */

        public void html(String title, String css, String body) {
            mime("text/html; charset=utf-8");
            this.body = "<!doctype html><html><meta charset=\"UTF-8\" />" +
                    "<title>" + title + "</title><style>" + css + "</style>" +
                    "</head><body>" + body + "</body></html>";
        }

        /**
         * Setter fuer {@link Router.Data#body}
         *
         * @param body Setzt den Wert auf den der uebergeben wurde.
         */

        public void body(String body) {
            this.body = body;
        }

        /**
         * Setter fuer {@link Router.Data#mime}
         *
         * @param mime Setzt den Wert auf den der uebergeben wurde.
         */

        public void mime(String mime) {
            this.mime = mime;
        }

        /**
         * Setter fuer {@link Router.Data#error}
         *
         * @param error Setzt den Wert auf den der uebergeben wurde.
         */

        public void error(String error) {
            this.error = error;
        }

        /**
         * Setter fuer {@link Router.Data#stream}
         *
         * @param stream Setzt den Wert auf den der uebergeben wurde.
         */

        public void stream(InputStream stream) {
            this.stream = stream;
        }
    }

    /**
     * Die ErrorResponse Klasse implementiert das Atwprten auf einen HTTP-Request mit einer Fehlerantwort. Sie ist Teil
     * von Router.
     *
     * @author Phillip Kaludercic
     * @version 1.0
     */

    public static class ErrorResponse implements Response {
        /** Speichert eine vorgefertigte Fehlermeldung. */
        final private Data d = new Data();

        /**
         * Konstruktor welcher den Fehler generiert.
         *
         * @param status Fehlercode des HTTP-Requests.
         * @param txt    Fehlermeldung
         */
        public ErrorResponse(NanoHTTPD.Response.Status status, String txt) {
            d.status(status);
            d.error(txt);
        }

        /**
         * Generiert Antwort als DataResponse und dieses zurueckgeben.
         *
         * @param session Hier muss die aktuelle session gegeben werden, ist jedoch hier irrelevant.
         */

        @Override
        public Data reply(IHTTPSession session) {
            return d;
        }
    }

    /**
     * Die StaticResponse Klasse implementiert das Antworten auf einen HTTP-Request mit einer statischen Datei. Sie ist
     * Teil von Router.
     *
     * @author Phillip Kaludercic
     * @version 1.0
     */

    public class StaticResponse implements Response {
        /** Wird zur Response generation gebraucht */
        final private String uri;
        /** Wird zur Response generation gebraucht */
        final private String mime;

        /**
         * Konstruktor fuer die Klasse.
         *
         * @param uri  Ort der statischen Datei.
         * @param mime Hier muss die Art der Datei angegeben werden.
         */

        public StaticResponse(String uri, String mime) {
            this.uri = uri;
            this.mime = mime;
        }

        /**
         * Generiert Antwort als DataResponse und dieses zurueckgeben.
         *
         * @param session Hier muss die aktuelle session gegeben werden, ist jedoch hier irrelevant.
         */

        public Data reply(IHTTPSession session) {
            Data d = new Data();
            d.stream(Router.class.getResourceAsStream(uri));
            d.mime(mime);
            return d;
        }
    }
}