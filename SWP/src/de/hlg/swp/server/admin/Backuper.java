package de.hlg.swp.server.admin;

import de.hlg.swp.server.Router;
import de.hlg.swp.server.Router.Response;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;

import java.nio.charset.Charset;
import java.util.Base64;

import static de.hlg.swp.Main.adm;
import static de.hlg.swp.Main.bcp;

/**
 * Diese Klasse ist fuer das Ausloesen eines bewusst durchgefuehrten Backups zustaendig.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Backuper implements Response {

    /**
     * Die Methode ueberprueft das Passwort und fuehrt bei korrektem Passwort ein Backup durch mithilfe von
     *
     * @param req Mitgelieferte Daten die durch den Server fuer die Methode zur Verfuegung gestellt werden.
     */

    @Override
    public Router.Data reply(IHTTPSession req) {
        Router.Data d = new Router.Data();

        if (!req.getHeaders().containsKey("authorization")) {
            d.put("WWW-Authenticate", "Basic realm=\"Admin Login\"");
            d.status(NanoHTTPD.Response.Status.UNAUTHORIZED);
            return d;
        }
        String auth = req.getHeaders().get("authorization");
        if (auth != null && auth.startsWith("Basic"))
            if (!adm.gebeAdmin().passOK((new String(Base64.getDecoder().decode(auth.substring("Basic".length()).trim()),
                    Charset.forName("UTF-8")).split(":", 2)[1]))) {
                d.status(NanoHTTPD.Response.Status.FORBIDDEN);
                d.body("Passwort falsch");
                return d;
            }


        bcp.fuehreBackupDurch();
        d.put("Refresh", "3; url=/");
        d.body("Backup erfolgreich.");
        return d;
    }
}
