package de.hlg.swp.server.admin;

import de.hlg.swp.server.Admin;
import de.hlg.swp.server.Router;
import fi.iki.elonen.NanoHTTPD;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;

import static de.hlg.swp.Main.adm;

/**
 * Diese Klasse generiert die Schuelerliste fuer die Adminoberflaeche, sortiert nach Klassen, zeigt zusaetzlich die Passwoerter an.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class WunschAnzeiger implements Router.Response {

    /** Generiert eine Tabellenreihe. */
    private static final String reiheFormat;
    /** Generiert einen Tabellenkopf */
    private static final String kopfFormat;

    static {
        List<String> spalten = new ArrayList<>();
        spalten.add("Name");
        spalten.add("Vorname");
        spalten.add("Geschl.");
        spalten.add("Hat gewahlt?");
        spalten.add("1. Erstwahl");
        spalten.add("2. Erstwahl");
        spalten.add("3. Erstwahl");
        spalten.add("4. Erstwahl");
        spalten.add("1. Alt-wahl");
        spalten.add("2. Alt-wahl");
        spalten.add("Nicht-Wahl");

        kopfFormat = "<tr>" + spalten.stream()
                .map(s -> "<th>" + s + "</th>")
                .reduce("", String::concat) + "</tr>";
        reiheFormat = "<tr>" + spalten.stream()
                .map(s -> "<td>%s</td>")
                .reduce("", String::concat) + "</tr>";
    }

    /**
     * Diese Methode generiert die Liste mit den Schuelern und ihren zugehoerigen Wuenschen. Dies wird jedoch nur ausgefuehrt, wenn das Passwort korrekt ist.
     *
     * @param req Enthaelt die Anfrage die an den Server zurueckgegeben wird.
     * @return Die Wunschtabelle als HTML-Tabelle.
     */

    @Override
    public Router.Data reply(NanoHTTPD.IHTTPSession req) {
        Router.Data d = new Router.Data();
        if (!req.getHeaders().containsKey("authorization")) {
            d.put("WWW-Authenticate", "Basic realm=\"Admin Login\"");
            d.status(NanoHTTPD.Response.Status.UNAUTHORIZED);
            return d;
        }
        String auth = req.getHeaders().get("authorization");
        if (auth != null && auth.startsWith("Basic"))
            if (!adm.gebeAdmin().passOK((new String(Base64.getDecoder().decode(auth.substring("Basic".length()).trim()),
                    Charset.forName("UTF-8")).split(":", 2)[1]))) {
                d.status(NanoHTTPD.Response.Status.FORBIDDEN);
                d.body("Passwort falsch");
                return d;
            }

        if (adm.getStatus() != Admin.FERTIG) {
            d.status(NanoHTTPD.Response.Status.CONFLICT);
            d.body("Wahl/Optimization noch nicht fertig");
            return d;
        }

        d.html("Wuensche",
                "<h1>Wuensche <a href=\"/csv?wunsch=1\">link</a></h1><table frame=void border=\"\">" + kopfFormat +
                adm.gebeDaten().gebeSchueler().stream()
                        .sorted(Comparator.comparing(s -> s.gebeNachnamen() + s.gebeVornamen()))
                        .map(s -> String.format(reiheFormat,
                                s.gebeNachnamen(),
                                s.gebeVornamen(),
                                s.istWeiblich() ? "Weibl." : "Maenn.",
                                s.hatGewahlt() ? "Ja" : "Nein",
                                s.gebeErstwahlNachIndex(0),
                                s.gebeErstwahlNachIndex(1),
                                s.gebeErstwahlNachIndex(2),
                                s.gebeErstwahlNachIndex(3),
                                s.gebeAltwahlNachIndex(0),
                                s.gebeAltwahlNachIndex(1),
                                s.gebeNichtsport()))
                        .reduce("", String::concat) +
                        "</table>");
        return d;
    }
}
