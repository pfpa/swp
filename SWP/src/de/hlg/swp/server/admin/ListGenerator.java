package de.hlg.swp.server.admin;

import de.hlg.swp.models.Schueler;
import de.hlg.swp.server.Router;
import de.hlg.swp.server.Router.Response;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;

import static de.hlg.swp.Main.adm;

/**
 * Diese Klasse stellt fuer den Administrator die Liste in HTML-Format bereit, welche die Schueler enthaelt und ob
 * er/sie gewaehlt haben.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class ListGenerator implements Response {

    /**
     * Hier wird die Liste generiert, welche alle relevanten Informationen enthaelt, sie wird in HTML-Form
     * zurueckgegeben.
     *
     * @param req Mitgelieferte Daten die durch den Server fuer die Methode zur Verfuegung gestellt werden.
     */

    @Override
    public Router.Data reply(IHTTPSession req) {
        boolean nichtwahl = req.getParms().containsKey("nichtwahl");
        String resp = "";

        for (Schueler s : adm.gebeDaten().gebeSchueler())
            if (!nichtwahl || !s.hatGewahlt()) // wenn alle angezeit werden sollen, oder andernfalls nur nicht-wahler
                resp += String.format("%s,%s,%s,%s\n",
                        s.gebeNachnamen(),
                        s.gebeVornamen(),
                        s.gebeKlasse(),
                        s.hatGewahlt() ? "Ja" : "Nein");

        Router.Data d = new Router.Data();
        d.body(resp);
        return d;
    }

}
