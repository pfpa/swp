package de.hlg.swp.server.admin;

import de.hlg.swp.server.Admin;
import de.hlg.swp.server.Router;
import de.hlg.swp.server.Router.Response;
import de.hlg.swp.utils.Optimierer;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;

import java.nio.charset.Charset;
import java.util.Base64;

import static de.hlg.swp.Main.adm;

/**
 * Klasse die fuer das Beenden des Schuelerservers zustaendig ist. Danach fuehrt die Klasse die Optimisation aus.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Terminator implements Response {

    /**
     * Die Methode beendet die Schuelerwahl und fuehrt im Anschluss die Optimisation aus. Dies kann nur erfolgreich
     * sein, wenn die Schuelerwahl schon vorher gestartet wurde.
     *
     * @param req Mitgelieferte Daten die durch den Server fuer die Methode zur Verfuegung gestellt werden.
     */

    @Override
    public Router.Data reply(IHTTPSession req) {
        Router.Data d = new Router.Data();

        if (!req.getHeaders().containsKey("authorization")) {
            d.put("WWW-Authenticate", "Basic realm=\"Admin Login\"");
            d.status(NanoHTTPD.Response.Status.UNAUTHORIZED);
            return d;
        }
        String auth = req.getHeaders().get("authorization");
        if (auth != null && auth.startsWith("Basic"))
            if (!adm.gebeAdmin().passOK((new String(Base64.getDecoder().decode(auth.substring("Basic".length()).trim()),
                    Charset.forName("UTF-8")).split(":", 2)[1]))) {
                d.status(NanoHTTPD.Response.Status.FORBIDDEN);
                d.body("Passwort falsch");
                return d;
            }

        if (adm.getStatus() != Admin.IN_DER_WAHL) {
            d.body("Wahl noch nicht begonnen/fertig.");
            d.status(NanoHTTPD.Response.Status.CONFLICT);
            return d;
        }

        adm.gebeSchuelerServer().halt();
        adm.setzeStatus(Admin.IN_DER_OPTIMIZ);
        new Thread() {
            @Override
            public void run() {
                try {
                    adm.setzeOptimiz(new Optimierer(
                            adm.gebeDaten().gebeSchueler(),
                            adm.gebeDaten().gebeKurse()));
                    adm.setzeStatus(Admin.FERTIG);
                } catch (Exception e) {
                    e.printStackTrace();
                    adm.setzeStatus(Admin.SERVER_FEHLER);
                }
            }
        }.start();

        d.put("Refresh", "3; url=/");
        d.body("Erfolgreich beendet; Schuelerwahl wurde beendet");
        return d;
    }

}
