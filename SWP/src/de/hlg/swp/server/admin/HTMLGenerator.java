package de.hlg.swp.server.admin;

import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Sportkurs;
import de.hlg.swp.models.Teilnahme;
import de.hlg.swp.server.Admin;
import de.hlg.swp.server.Router;
import de.hlg.swp.server.Router.Response;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;

import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;

import static de.hlg.swp.Main.additumKategNachSem;
import static de.hlg.swp.Main.adm;

/**
 * Diese Klasse generiert die Listen in HTML und gibt diese im Browser aus.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class HTMLGenerator implements Response {

    private static final String reiheFormat, kopfFormat, reiheKurzFormat, kopfKurzFormat;

    static {
        List<String> spalten = new ArrayList<>();
        spalten.add("Name");
        spalten.add("Vorname");
        spalten.add("Klasse");
        spalten.add("Geschl.");
        spalten.add("Additum?");
        spalten.add("Wahler?");

        reiheKurzFormat = "<tr>" + spalten.stream()
                .map(s -> "<td>%s</td>")
                .reduce("", String::concat) + "</tr>";
        kopfKurzFormat = "<tr>" + spalten.stream()
                .map(s -> "<th>" + s + "</th>")
                .reduce("", String::concat) + "</tr>";

        spalten.add("1. Sem.");
        spalten.add("2. Sem.");
        spalten.add("3. Sem.");
        spalten.add("4. Sem.");

        kopfFormat = "<tr>" + spalten.stream()
                .map(s -> "<th>" + s + "</th>")
                .reduce("", String::concat) + "</tr>";
        reiheFormat = "<tr>" + spalten.stream()
                .map(s -> "<td>%s</td>")
                .reduce("", String::concat) + "</tr>";
    }

    /**
     * Diese Methode generiert die Listen nach Schuelern, nach Semestern und Kursen geordnet und gibt sie aus.
     *
     * @param req Mitgelieferte Daten die durch den Server fuer die Methode zur Verfuegung gestellt werden.
     */

    @Override
    public Router.Data reply(IHTTPSession req) {
        Router.Data d = new Router.Data();
        if (!req.getHeaders().containsKey("authorization")) {
            d.put("WWW-Authenticate", "Basic realm=\"Admin Login\"");
            d.status(NanoHTTPD.Response.Status.UNAUTHORIZED);
            return d;
        }
        String auth = req.getHeaders().get("authorization");
        if (auth != null && auth.startsWith("Basic"))
            if (!adm.gebeAdmin().passOK((new String(Base64.getDecoder().decode(auth.substring("Basic".length()).trim()),
                    Charset.forName("UTF-8")).split(":", 2)[1]))) {
                d.status(NanoHTTPD.Response.Status.FORBIDDEN);
                d.body("Passwort falsch");
                return d;
            }

        if (adm.getStatus() != Admin.FERTIG) {
            d.status(NanoHTTPD.Response.Status.CONFLICT);
            d.body("Wahl/Optimization noch nicht fertig");
            return d;
        }

        Collection<Teilnahme> teilnahmen = adm.gebeOptimiz().gebeResultate();
        Function<Schueler, String> genReihe = (s -> String.format(reiheFormat,
                s.gebeNachnamen(),
                s.gebeVornamen(),
                s.gebeKlasse(),
                s.istWeiblich() ? "Weiblich" : "Mänlich",
                s.machtAditum() ? "Ja" : "Nein",
                s.hatGewahlt() ? "Nein" : "Ja",
                s.gebeTeilnahmeNachSemester(0, teilnahmen),
                s.gebeTeilnahmeNachSemester(1, teilnahmen),
                s.gebeTeilnahmeNachSemester(2, teilnahmen),
                s.gebeTeilnahmeNachSemester(3, teilnahmen)
        )), genKurzReihe = (s -> String.format(reiheKurzFormat,
                s.gebeNachnamen(),
                s.gebeVornamen(),
                s.gebeKlasse(),
                s.istWeiblich() ? "Weiblich" : "Mänlich",
                s.machtAditum() ? "Ja" : "Nein",
                s.hatGewahlt() ? "Nein" : "Ja"
        ));
        Comparator<Schueler> alphaSort = Comparator.comparing(s -> s.gebeNachnamen() + s.gebeVornamen());

        d.html("Resultate", "<h2>Gesamtliste <a href=\"/csv?gesamt=1\">link</a></h2><table frame=void border=\"\">" + kopfFormat +
                adm.gebeDaten().gebeSchueler().stream().parallel()
                        .sorted(alphaSort)
                        .map(genReihe)
                        .reduce("", String::concat) +
                "</table><h2>Semester und Kursliste</h2>" +
                IntStream.range(0, additumKategNachSem.length).parallel().mapToObj(sem ->
                        "<h3>Semester " + (sem + 1) + "</h3>" + adm.gebeDaten().gebeKurse().stream().parallel()
                                .filter(k -> k.gebeTeilnehmerZahl(teilnahmen, sem) > 0)
                                .sorted(Comparator.comparing(Sportkurs::gebeName))
                                .map(k -> "<h4>" + k.gebeName() +
                                        " <a href=\"/csv?sem=" + sem + "&kid=" + k.gebeID() + "\">link</a></h4>" +
                                        "<table frame=void border=\"\">" + kopfKurzFormat +
                                        k.gebeTeilnehmer(teilnahmen, sem).parallel()
                                                .sorted(alphaSort)
                                                .map(genKurzReihe)
                                                .reduce("", String::concat) +
                                        "</table>")
                                .reduce("", String::concat))
                        .reduce("", String::concat));
        return d;
    }

}
