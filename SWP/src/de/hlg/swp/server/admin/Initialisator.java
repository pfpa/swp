package de.hlg.swp.server.admin;

import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Sportkurs;
import de.hlg.swp.server.Admin;
import de.hlg.swp.server.Router;
import de.hlg.swp.server.Router.Response;
import de.hlg.swp.server.Server;
import de.hlg.swp.utils.Daten;
import de.hlg.swp.utils.Leser;
import fi.iki.elonen.NanoFileUpload;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.List;
import java.util.Set;

import static de.hlg.swp.Main.adm;
import static de.hlg.swp.Main.conf;

/**
 * Diese Klasse ist fuer die Vorbereitungen vor der Wahl zustaendig. Die Klasse verarbeitet Kurs- und Klassenliste.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Initialisator implements Response {

    /**
     * Methode die fuer das Einlesen der Kurs- und Schuelerdatei zustaendig ist, sie validiert dabei gleichzeitig ob
     * damit eine Kurswahl moeglich ist.
     *
     * @param req Mitgelieferte Daten die durch den Server fuer die Methode zur Verfuegung gestellt werden.
     */

    @Override
    public Router.Data reply(IHTTPSession req) {
        Router.Data d = new Router.Data();

        if (!req.getHeaders().containsKey("authorization")) {
            d.put("WWW-Authenticate", "Basic realm=\"Admin Login\"");
            d.status(NanoHTTPD.Response.Status.UNAUTHORIZED);
            return d;
        }
        String auth = req.getHeaders().get("authorization");
        if (auth != null && auth.startsWith("Basic"))
            if (!adm.gebeAdmin().passOK((new String(Base64.getDecoder().decode(auth.substring("Basic".length()).trim()),
                    Charset.forName("UTF-8")).split(":", 2)[1]))) {
                d.status(NanoHTTPD.Response.Status.FORBIDDEN);
                d.body("Passwort falsch");
                return d;
            }

        NanoFileUpload uploader = new NanoFileUpload(new DiskFileItemFactory());

        if (adm.getStatus() != Admin.NOCH_NICHT_ANGEF) {
            d.status(NanoHTTPD.Response.Status.CONFLICT);
            d.body("Server hat schon dateien empfangen");
            return d;
        }

        if (!NanoFileUpload.isMultipartContent(req)) {
            d.status(NanoHTTPD.Response.Status.BAD_REQUEST);
            d.body("Kein Multipart content");
            return d;
        }

        Set<Schueler> schueler = null;
        List<Sportkurs> kurse = null;
        try {
            for (FileItem fi : uploader.parseRequest(req)) {
                if (fi.getFieldName().equals("schueler"))
                    schueler = Leser.leseSchueler(fi.getInputStream());
                if (fi.getFieldName().equals("kurse")) {
                    kurse = Leser.leseKurse(fi.getInputStream());
                    for (Sportkurs k : kurse)
                        if (!k.istVerteilungValide()) {
                            d.status(NanoHTTPD.Response.Status.NOT_ACCEPTABLE);
                            d.body("Fehler in dateien, invalide Kursverteilung.");
                            return d;
                        }
                }
                if (fi.getFieldName().equals("max")) {
                    String max = fi.getString();
                    if (!max.matches("^\\d+$")) {
                        d.status(NanoHTTPD.Response.Status.BAD_REQUEST);
                        d.body("Max. Kurszahl invalide");
                        d.status(NanoHTTPD.Response.Status.BAD_REQUEST);
                        return d;
                    }
                    conf.MAX_KURS_ZAHL = Integer.parseUnsignedInt(max);
                }
            }
        } catch (FileUploadException fue) {
            d.status(NanoHTTPD.Response.Status.INTERNAL_ERROR);
            d.body("Fehler beim Vernehmen der Datein.");
            return d;
        } catch (IOException ioe) {
            d.status(NanoHTTPD.Response.Status.INTERNAL_ERROR);
            d.body("IO Fehler beim lesen: " + ioe.getLocalizedMessage());
            return d;
        }

        if (schueler == null || schueler.size() < 1 || kurse == null || kurse.size() < 1) {
            d.status(NanoHTTPD.Response.Status.NOT_ACCEPTABLE);
            d.body("Fehler in dateien, zu wenige schueler oder kurse");
            return d;
        }

        adm.setzeDaten(new Daten(schueler, kurse));
        adm.setzeStatus(Admin.IN_DER_WAHL);
        adm.setzeUmfrageStart(System.currentTimeMillis());
        adm.setzeSchuelerServer(new Server());

        d.put("Refresh", "3; url=/schuelerlist");
        d.body("Setup erfolgreich; Schuelerwahl kann beginnen; Sie werden gleich zu der Schuelerliste weitergeleitet");
        return d;
    }
}
