package de.hlg.swp.server.admin;

import de.hlg.swp.models.Schueler;
import de.hlg.swp.server.Router;
import de.hlg.swp.server.Router.Response;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;

import java.nio.charset.Charset;
import java.util.*;

import static de.hlg.swp.Main.adm;

/**
 * Diese Klasse generiert die Schuelerliste fuer die Adminoberflaeche, sortiert nach Klassen.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class SchuelerListGenerator implements Response {

    /** Generiert eine Tabellenreihe. */
    private static final String reiheFormat;
    /** Generiert einen Tabellenkopf */
    private static final String kopfFormat;
    /** enthaelt die Breite einer Spalte */
    private static final int breite;

    static {
        List<String> spalten = new ArrayList<>();
        spalten.add("Name");
        spalten.add("Vorname");
        spalten.add("Benutzername");
        spalten.add("Passwort");

        reiheFormat = "<tr>" + spalten.stream()
                .map(s -> "<td> %s</td>")
                .reduce("", String::concat) + "</tr>";
        kopfFormat = "<tr>" + spalten.stream()
                .map(s -> "<th>" + s + "</th>")
                .reduce("", String::concat) + "</tr>";
        breite = (int) (100d / spalten.size());
    }

    /**
     * Methode die die Schuelerliste implementiert. Dies wird nur bei korrektem Passwort des Admins ausgefuehrt und
     * maximal 5 min nach dem Starten des Schuelerservers. Dannach kann die Liste nicht mehr angezeigt werden.
     *
     * @param req Mitgelieferte Daten die durch den Server fuer die Methode zur Verfuegung gestellt werden.
     */

    @Override
    public Router.Data reply(IHTTPSession req) {
        Router.Data d = new Router.Data();

        if (!req.getHeaders().containsKey("authorization")) {
            d.put("WWW-Authenticate", "Basic realm=\"Admin Login\"");
            d.status(NanoHTTPD.Response.Status.UNAUTHORIZED);
            return d;
        }
        String auth = req.getHeaders().get("authorization");
        if (auth != null && auth.startsWith("Basic"))
            if (!adm.gebeAdmin().passOK((new String(Base64.getDecoder().decode(auth.substring("Basic".length()).trim()),
                    Charset.forName("UTF-8")).split(":", 2)[1]))) {
                d.status(NanoHTTPD.Response.Status.FORBIDDEN);
                d.body("Passwort falsch");
                return d;
            }

        if ((System.currentTimeMillis() - adm.gebeUmfrageStart()) > 1000 * 60 * 5) {// nach funf minuten, nicht mehr zugriffbar
            d.status(NanoHTTPD.Response.Status.FORBIDDEN);
            d.body("Schueler Liste kann/darf nicht (mehr) angezeigt werden.");
            return d;
        }

        Collection<Schueler> sch = adm.gebeDaten().gebeSchueler();
        d.html("Schueler Liste", "td { width: " + breite + "%;}", sch.stream()
                .map(Schueler::gebeKlasse)
                .distinct()
                .sorted(String::compareTo)
                .map(k -> "<h1>Klasse " + k + "</h1><table style=\"width: 100%\" frame=void border=\"\">" +
                        kopfFormat + sch.stream()
                        .filter(s -> s.gebeKlasse().equals(k))
                        .sorted(Comparator.comparing(s -> s.gebeNachnamen() + s.gebeVornamen()))
                        .map(s -> String.format(reiheFormat,
                                s.gebeNachnamen(),
                                s.gebeVornamen(),
                                s.gebeUsername(),
                                "<code>" + s.gebePasswd() + "</code>"
                        ))
                        .reduce("", String::concat) +
                        "</table>")
                .reduce("", String::concat));
        return d;
    }
}
