package de.hlg.swp.server.admin;

import de.hlg.swp.models.Sportkurs;
import de.hlg.swp.models.Teilnahme;
import de.hlg.swp.server.Router;
import fi.iki.elonen.NanoHTTPD;

import java.nio.charset.Charset;
import java.util.*;

import static de.hlg.swp.Main.adm;

/**
 * Diese Klasse ist fuer das erstellen einer CSV zustaendig.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class CSVGenerator implements Router.Response {

    private static final String reiheFormat,
            kopfFormat,
            reiheKurzFormat,
            kopfKurzFormat,
            reiheWunschFormat,
            kopfWunschFormat;

    static {
        List<String> spalten = new ArrayList<>();
        Collections.addAll(spalten);
        spalten.add("Name");
        spalten.add("Vorname");
        spalten.add("Klasse");
        spalten.add("Geschl.");
        spalten.add("Additum?");
        spalten.add("Wahler?");

        reiheKurzFormat = spalten.stream()
                .map(s -> "%s; ")
                .reduce("", String::concat)
                .replaceAll("; $", "\n");
        kopfKurzFormat = spalten.stream()
                .map(s -> s + "; ")
                .reduce("", String::concat)
                .replaceAll("; $", "\n");
        List<String> wunsch = new ArrayList<>(spalten);

        spalten.add("1. Sem.");
        spalten.add("2. Sem.");
        spalten.add("3. Sem.");
        spalten.add("4. Sem.");

        reiheFormat = spalten.stream()
                .map(s -> "%s; ")
                .reduce("", String::concat)
                .replaceAll("; $", "\n");
        kopfFormat = spalten.stream()
                .map(s -> s + "; ")
                .reduce("", String::concat)
                .replaceAll("; $", "\n");

        wunsch.add("1. Wunsch");
        wunsch.add("2. Wunsch");
        wunsch.add("3. Wunsch");
        wunsch.add("4. Wunsch");
        wunsch.add("1. Altern.");
        wunsch.add("2. Altern.");
        wunsch.add("Nichtwahl");

        reiheWunschFormat = wunsch.stream()
                .map(s -> "%s; ")
                .reduce("", String::concat)
                .replaceAll("; $", "\n");
        kopfWunschFormat = wunsch.stream()
                .map(s -> s + "; ")
                .reduce("", String::concat)
                .replaceAll("; $", "\n");
    }

    @Override
    public Router.Data reply(NanoHTTPD.IHTTPSession req) {
        Router.Data d = new Router.Data();
        if (!req.getHeaders().containsKey("authorization")) {
            d.put("WWW-Authenticate", "Basic realm=\"Admin Login\"");
            d.status(NanoHTTPD.Response.Status.UNAUTHORIZED);
            return d;
        }
        String auth = req.getHeaders().get("authorization");
        if (auth != null && auth.startsWith("Basic"))
            if (!adm.gebeAdmin().passOK((new String(Base64.getDecoder().decode(auth.substring("Basic".length()).trim()),
                    Charset.forName("UTF-8")).split(":", 2)[1]))) {
                d.status(NanoHTTPD.Response.Status.FORBIDDEN);
                d.body("Passwort falsch");
                return d;
            }

        Map<String, String> params = req.getParms();
        Collection<Teilnahme> teilnahmen = adm.gebeOptimiz().gebeResultate();

        String filename;
        if (params.containsKey("gesamt")) {
            filename = "gesamtliste.csv";
            d.body(kopfFormat + adm.gebeDaten().gebeSchueler().stream()
                    .sorted(Comparator.comparing(s -> s.gebeNachnamen() + s.gebeVornamen()))
                    .map(s -> String.format(reiheFormat,
                            s.gebeNachnamen(),
                            s.gebeVornamen(),
                            s.gebeKlasse(),
                            s.istWeiblich() ? "W" : "M",
                            s.machtAditum() ? "Ja" : "Nein",
                            s.hatGewahlt() ? "Nein" : "Ja",
                            s.gebeTeilnahmeNachSemester(0, teilnahmen),
                            s.gebeTeilnahmeNachSemester(1, teilnahmen),
                            s.gebeTeilnahmeNachSemester(2, teilnahmen),
                            s.gebeTeilnahmeNachSemester(3, teilnahmen)))
                    .reduce("", String::concat));
        } else if (params.containsKey("wunsch")) {
            filename = "wunschliste.csv";
            d.body(kopfWunschFormat + adm.gebeDaten().gebeSchueler().stream()
                    .sorted(Comparator.comparing(s -> s.gebeNachnamen() + s.gebeVornamen()))
                    .map(s -> String.format(reiheWunschFormat,
                            s.gebeNachnamen(),
                            s.gebeVornamen(),
                            s.gebeKlasse(),
                            s.istWeiblich() ? "W" : "M",
                            s.machtAditum() ? "Ja" : "Nein",
                            s.hatGewahlt() ? "Nein" : "Ja",
                            s.gebeErstwahlNachIndex(0),
                            s.gebeErstwahlNachIndex(1),
                            s.gebeErstwahlNachIndex(2),
                            s.gebeErstwahlNachIndex(3),
                            s.gebeAltwahlNachIndex(0),
                            s.gebeAltwahlNachIndex(1),
                            s.gebeNichtsport()))
                    .reduce("", String::concat));
        } else {
            if (!params.containsKey("sem") || !params.containsKey("kid")) {
                d.status(NanoHTTPD.Response.Status.BAD_REQUEST);
                d.error("<code>sem</code> oder <code>kid</code> Parameter fehlen.");
                return d;
            }

            int sem = Integer.parseUnsignedInt(params.get("sem")),
                    kid = Integer.parseUnsignedInt(params.get("kid"));

            Sportkurs kurs = Sportkurs.gebeNachID(kid);
            if (kurs == null) {
                d.error("Invalide Kurs-ID");
                d.status(NanoHTTPD.Response.Status.BAD_REQUEST);
                return d;
            }

            filename = "sem" + (sem + 1) + "_kurs_" + kurs.gebeName().toLowerCase() + ".csv\"";
            d.body(kopfKurzFormat + teilnahmen.stream()
                    .filter(t -> t.gebeKurs().gebeID() == kid)
                    .filter(t -> t.istSemester(sem))
                    .map(Teilnahme::gebeSchueler)
                    .sorted(Comparator.comparing(s -> s.gebeNachnamen() + s.gebeVornamen()))
                    .map(s -> String.format(reiheKurzFormat,
                            s.gebeNachnamen(),
                            s.gebeVornamen(),
                            s.gebeKlasse(),
                            s.istWeiblich() ? "W" : "M",
                            s.machtAditum() ? "Ja" : "Nein",
                            s.hatGewahlt() ? "Nein" : "Ja"))
                    .reduce("", String::concat));
        }

        d.mime("text/csv; charset=utf-8");
        d.put("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return d;
    }
}
