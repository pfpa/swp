package de.hlg.swp.server;

import de.hlg.swp.server.server.OptionenGenerator;
import de.hlg.swp.server.server.Wahlverarbeiter;

import static de.hlg.swp.Main.conf;
import static de.hlg.swp.Main.log;
import static fi.iki.elonen.NanoHTTPD.Method.GET;
import static fi.iki.elonen.NanoHTTPD.Method.POST;

/**
 * Diese Klasse ist fuer die Steuerung der Schueler GUI zustaendig.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Server {

    /** Dies ist das Objekt was letzendlich den Schuelerserver darstellt. */
    final private Router server;

    /** Server starten und Festlegen der Sportarten fuer die Schueler auf dem GUI */
    public Server() {
        server = new Router(conf.PORT, "sch");

        try {
            // Statische Datein
            String DIR = "/public/static";
            server.addRouteStatic(GET, "/", DIR + "/Wahlseite.html", "text/html");
            server.addRouteStatic(GET, "/js/jquery.js", DIR + "/js/jquery.js", "text/html");
            server.addRouteStatic(GET, "/js/schueler.js", DIR + "/js/schueler.js", "application/x-javascript");
            server.addRouteStatic(GET, "/css/style.css", DIR + "/css/style.css", "text/css");

            // Bilder
            server.addRouteStatic(GET, "/img/logo.jpg", DIR + "/img/logo.jpg", "image/jpeg");
            server.addRouteStatic(GET, "/img/sportarten.svg", DIR + "/img/sportarten.svg", "image/svg+xml");

            server.addRoute(POST, "/submit", new Wahlverarbeiter());
            server.addRoute(GET, "/api/wahl", new OptionenGenerator());

            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Schuelerserver erfolgreich auf http://[ip]:" + conf.PORT + "/ gestarted!");
    }

    /** Methode zum anhalten des Server */

    public void halt() {
        server.halt();
    }

    /**
     * Getter fuer {@link Server#server}
     *
     * @return Gibt das Serverobjekt zurueck.
     */

    public Router getServer() {
        return server;
    }
}
