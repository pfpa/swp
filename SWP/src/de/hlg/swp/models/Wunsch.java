package de.hlg.swp.models;

/**
 * Diese Klasse enthaelt einen Wunsch, eines Schuelers.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Wunsch {

    /** Statische Konstante die angibt ob ein Wunsche eine Erstwahl ist. */
    public static final int ERSTWAHL = 1;
    /** Statische Konstante die angibt ob ein Wunsche eine Alternativwahl ist. */
    public static final int ALTWAHL = 1 << 1;
    /** Statische Konstante die angibt ob ein Wunsche eine Ausschlusswahl ist. */
    public static final int NICHTWAHL = 1 << 2;

    /** Gibt an welcher Kurs geewaehlt wurde. */
    final private Sportkurs sportkurs;
    /** Enthealt den Typ des gewaehlten Kurses. */
    final private int typ;

    /**
     * Standardkonstuktor der den gewaehlten Kurs in diesem Objekt initialisiert.
     *
     * @param sportkurs Gewaehlter Sportkurs.
     * @param typ Typ des gewaehlten Sportkurses an.
     */

    public Wunsch(Sportkurs sportkurs, int typ) {
        this.sportkurs = sportkurs;
        this.typ = typ;
    }

    /**
     * Konstruktor bei der den gewaehlten Kurs in diesem Objekt initalisiert. Hierbei wird jedoch den String in einen Integer und schliesslich in das Sportkursobjekt umgewandelt.
     *
     * @param id Gewaehlter Sportkurs
     * @param typ Typ des gewaehlten Sportkurses.
     * @throws NumberFormatException Die ausgeloest wird, wenn der String keine valide Zahl enthaelt
     */

    public Wunsch(String id, int typ) throws NumberFormatException {
        this.sportkurs = Sportkurs.gebeNachID(Integer.parseUnsignedInt(id));
        this.typ = typ;
    }

    /**
     * Getter fuer den Kurs.
     *
     * @return Gibt den Sportkurs des Objektes zurueck.
     */

    public Sportkurs gebeKurs() {
        return sportkurs;
    }

    /**
     * Methode die vergleicht, ob ein Kurs identisch mit dem gewaehlten ist.
     *
     * @param k Zu vergleichender Kurs.
     * @return Gibt true, wenn die Kurse identisch sind und false, wenn nicht.
     */

    public boolean istKurs(Sportkurs k) {
        return sportkurs.equals(k);
    }

    /**
     * Getter fuer den Typ.
     *
     * @return Gibt den Kurstyp als int zurueck.
     */

    public int gebeTyp() {
        return typ;
    }

    /**
     * Mehtode die vergleicht, ob der Typ gleich dem gefrageten ist.
     *
     * @param typ Zu vergleichender Typ.
     * @return Gibt true, wenn die Typen identisch sind und false, wenn nciht.
     */

    public boolean istTyp(int typ) {
        return (this.typ & typ) != 0;
    }
}
