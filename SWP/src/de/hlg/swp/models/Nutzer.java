package de.hlg.swp.models;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static de.hlg.swp.Main.conf;
import static de.hlg.swp.Main.log;

/**
 * Diese Klasse steuert das Passwort einlesen und generieren.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Nutzer {

    /** Liste mit allen Woertern die fuer ein Passwort moeglich sind. */
    private static List<String> worter;

    /** Passwort fuer den jeweiligen Benutzer. */
    final private String pass;

    /**
     * Konstruktor der das Objekt mit intern generiertem Passwort initalisiert.
     */

    public Nutzer() {
        this.pass = gebePasswort();
    }

    /**
     * Konstruktor der das Objekt mit festgelegtem Passwort generiert.
     *
     * @param pass Gesetztes Passwort fuer den Benutzer.
     */

    public Nutzer(String pass) {
        this.pass = pass;
    }

    /**
     * Diese Methode liest die Datei passwd.txt von /public/static ein. Gespeichert werden die Woerter in der List
     * {@code worter}.
     */

    public static void configuriereWorter() {
        Reader br;
        if (conf.PASSWD_FILE != null)
            try {
                br = new FileReader(conf.PASSWD_FILE);
            } catch (IOException ioe) {
                log.severe("Fehler beim Lesen der Passwortdatei: " + ioe.getLocalizedMessage());
                System.exit(1);
                return;
            }
        else
            br = new InputStreamReader(Nutzer.class.getResourceAsStream("/public/passwd.txt"), Charset.forName("UTF-8"));
        try {
            worter = new BufferedReader(br)
                    .lines().parallel()
                    .map(String::trim)
                    .filter(l -> !l.isEmpty())
                    .collect(Collectors.toList());
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(1);
        }
        log.info("Worter Datei mit " + worter.size() + " Eintragen eingelesen.");
    }

    /**
     * Methode die ein Passwort fuer einen Benutzer/Schueler aus.
     *
     * @return pass gibt ein zufallsgeneriertes Passwort aus den Woertern der Datei passwd.txt aus. Das Passwort besteht
     * aus so vielen Woertern der Liste wie in {@code conf.PASSWD_DIFF} definiert.
     */

    private static String gebePasswort() {
        Collections.shuffle(worter);
        return worter.stream()
                .limit(conf.PASSWD_DIFF)
                .reduce(String::concat)
                .orElse("");
    }

    /**
     * Programm prüft passwort fuer benutzer richtig ist. Kontrolle faellt im debugging mode aus, immer wahr.
     *
     * @param passwd Passwort des Benutzers als String
     * @return boolean ob Passwort korrekt
     */

    public boolean passOK(String passwd) {
        return conf.DEBUG || pass.toLowerCase().equals(passwd.toLowerCase());
    }

    /**
     * Getter für die Objektvariable pass
     *
     * @return pass
     */

    public String gebePasswd() {
        return pass;
    }
}
