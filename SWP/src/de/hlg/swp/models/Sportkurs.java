package de.hlg.swp.models;

import com.eclipsesource.json.JsonObject;
import de.hlg.swp.utils.Optimierer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Stream;

import static de.hlg.swp.Main.additumKategNachSem;
import static de.hlg.swp.Main.adm;

/**
 * Diese Klasse Repraesentiert das Modell fuer einen Sportkurs.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Sportkurs {
    /** Statische Bitkonfiguration fuer Kurstypen. */
    public static final int EINZEL = 1, // << 0
            MANNSCHAFT = 1 << 1,
            ZUSATZ = 1 << 2,
            ALLE = EINZEL | MANNSCHAFT | ZUSATZ,
            ERZWINGE_MIN = 1 << 3,
            ERZWINGE_KAP = 1 << 4,
            WEIBLICH = 1 << 5,
            GZWANG = 1 << 6;

    /** JSON Backup keys */
    private static final String NAME = "n",
            MINIM = "m",
            KAPAT = "k",
            TYP = "t",
            FINSTA = "f";

    /** Benoetigte JSON keys um backup wiederherzustellen */
    private static Collection<String> jsonVars;

    static {
        jsonVars = new ArrayList<>();
        jsonVars.add(NAME);
        jsonVars.add(MINIM);
        jsonVars.add(KAPAT);
        jsonVars.add(TYP);
        jsonVars.add(FINSTA);
    }

    /** Variable die angibt welchen Namen ein Sportkurs hat. */
    final private String name;
    /** Variable die angibt was die minimale Teilnehmerzahl eines Sportkurses ist. */
    final private int minimal;
    /** Variable die angibt was die maximale Teilnehmerzahl eines Sportkurses ist. */
    final private int kapazitaet;
    /** Variable die angibt was fuer ein Typ (Einzel, Mannschaft, Zusatz) der Sportkurs hat. */
    final private int typ;
    /** Feld das angibt in welchen Semestern der Sportkurs stattfindet kann */
    final private boolean[] findetStatt;
    /** Feld das angibt in welchen Semestern der Sportkurs stattfinden darf */
    final private boolean[] darfStattfinden = new boolean[additumKategNachSem.length];

    /**
     * Sportkurskonstruktor welcher als Input Daten aus dem Programm bekommt.
     *
     * @param kursname    Der Kursname.
     * @param minimal     Minimale Anzahl an Schuelern die benoetigt werden, damit der Sportkurs stattfindet.
     * @param kapazitaet  Die Maximale Anzahl an Schuelern die an dem Sportkurs teilnehmen darf.
     * @param typ         Gibt an welcher Typ (Einzel, Mannschaft, Zusatz) der Sportkurs hat
     * @param weiblich    Gibt an (unter der Vorraussetzung das gzwang true ist), ob der Sportkurs nur fuer Maedchen
     *                    (true) oder Jungen (false) ist.
     * @param gzwang      Gibt an, ob ein Sportkurs nur fuer ein Geschlecht zugelassen ist.
     * @param findetStatt Gibt an ob der Sportkurs stattfindet.
     */

    public Sportkurs(String kursname, int minimal, int kapazitaet, int typ, boolean weiblich, boolean gzwang, boolean[] findetStatt) {
        this.name = kursname;
        this.minimal = minimal;
        this.kapazitaet = kapazitaet;
        this.typ = typ | (weiblich ? WEIBLICH : 0) | (gzwang ? GZWANG : 0);
        this.findetStatt = findetStatt;
    }

    /**
     * Sportkurskonstruktor der seine Daten aus einem JSON-Objekt des Backups erhaelt.
     *
     * @param o JSON-Objekt des Backups
     * @throws Exception Wird geworfen, wenn ein oder mehrere Keys fehlen im JSON-Objekt.
     */

    public Sportkurs(JsonObject o) throws Exception {
        if (!o.names().containsAll(jsonVars))
            throw new Exception("missing keys");
        name = o.getString(NAME, null);
        minimal = o.getInt(MINIM, -1);
        kapazitaet = o.getInt(KAPAT, -1);
        typ = o.getInt(TYP, -1);
        int fs = o.getInt(FINSTA, 0);
        findetStatt = new boolean[additumKategNachSem.length];
        for (int i = findetStatt.length - 1, c = 0; i >= 0; i--)
            findetStatt[c++] = (fs & (1 << i)) != 0;
    }

    /**
     * Gibt einen Sportkurs (welches vom Programm zentral gespeichert wurde) mit der spezifischen ID welcher als
     * Parameter angegeben wird.
     *
     * @param id ID des ausgegebenen Sportkurses
     * @return Sportkurs mit angegebener ID
     */

    public static Sportkurs gebeNachID(int id) {
        if (adm == null || adm.gebeDaten() == null) return null;
        return adm.gebeDaten().gebeKurse().stream()
                .filter(k -> k.gebeID() == id)
                .findAny()
                .orElse(null);
    }

    /**
     * Gibt einen Stream zurueck mit den Sportkursen in denen Teilnehmer mit Additum sind.
     *
     * @param sem Semester fuer welches gesucht werden soll.
     * @return Stream mit Kursen in denen Additumsteilnehmer sind.
     */

    public static Stream<Sportkurs> gebeAdditumKurse(int sem) {
        return adm.gebeDaten().gebeKurse().stream()
                .filter(k -> k.istTyp(additumKategNachSem[sem]))
                .filter(k -> k.gebeAdditumInteressierteZahl() > 0);
    }

    /**
     * Gibt die Zahl der Kurse mit Additumsteilnehmern.
     *
     * @param sem Beeschraenkt die Suche auf das gewuenschte Semester.
     * @return Gibt die absolute Anzahl an in Form eines {@code int}
     */

    public static int gebeAdditumKursZahl(int sem) {
        return (int) gebeAdditumKurse(sem).count();
    }

    /**
     * Gibt einen Stream mit den Top "N" Kursen zuruck.
     * <p>
     * Ob ein Sportkurs zu diesen zahlt, wird so ermittelt:
     * <p>
     * - Erst werden alle nicht stattfindenen kurse entfernt - Dann werden alle Additumskurse (fuer dieses semester)
     * entfernt - Danach wird nach popularitat sortiert - Zuletzt wird der Stream auf die N mitglieder begrenzt
     *
     * @param n   Kommandozeilenparameter
     * @param sem Bla
     * @return {@code Stream <Schueler>}
     */

    public static Stream<Sportkurs> gebeTopNKursen(int n, int sem) {
        return adm.gebeDaten().gebeKurse().stream()
                .filter(k -> k.findetStatt(sem)) // nur kurse die stattfinden
                .filter(k -> !k.istTyp(additumKategNachSem[sem]) && k.gebeAdditumInteressierteZahl() == 0) // ohne additums
                .sorted(Comparator.comparing(Sportkurs::gebeInteresierteZahl))
                .limit(n);
    }

    /**
     * Gibt die ID des Kurses zurueck.
     *
     * @return Gibt die ID als {@code int} zurueck
     */

    public int gebeID() {
        return adm.gebeDaten().gebeKurse().indexOf(this);
    }

    /**
     * Konvertiert das Sportkursobjekt als JSON-Objekt und gibt es zurueck.
     *
     * @return {@code JsonObject}
     */

    public JsonObject toJson() {
        int fs = 0;
        for (int i = findetStatt.length - 1; i >= 0; i--)
            fs |= (findetStatt[i] ? (1 << i) : 0);
        return new JsonObject()
                .add(NAME, name)
                .add(MINIM, minimal)
                .add(KAPAT, kapazitaet)
                .add(TYP, typ)
                .add(FINSTA, fs);
    }

    /**
     * Getter fuer {@link Sportkurs#minimal}.
     *
     * @return Gibt den Wert als {@code int} zurueck.
     */

    public int gebeMinimal() {
        return minimal;
    }

    /**
     * Getter fuer {@link Sportkurs#kapazitaet}.
     *
     * @return Gibt den Wert als {@code int} zurueck.
     */

    public int gebeKapazitaet() {
        return kapazitaet;
    }

    /**
     * Getter fuer {@link Sportkurs#typ}.
     *
     * @return Gibt den Wert als {@code int} zurueck.
     */

    public int gebeTyp() {
        return typ;
    }

    /**
     * Methode zum feststellen, ob der Sportkurs nur fuer Maedchen erlaubt ist.
     *
     * @return Gibt true, wenn nur Maedchen erlaubt sind, ansonsten false.
     */

    public boolean istNurWeiblich() {
        return istTyp(GZWANG) && istTyp(WEIBLICH);
    }

    /**
     * Methode zum feststellen, ob der Sportkurs nur fuer Jungen erlaubt ist.
     *
     * @return Gibt true, wenn nur Jungen erlaubt sind, ansonsten false.
     */

    public boolean istNurMaennlich() {
        return istTyp(GZWANG) && !istTyp(WEIBLICH);
    }

    /**
     * Getter fuer {@link Sportkurs#name}.
     *
     * @return Gibt den Namen des Sportkurses als String zurueck.
     */

    public String gebeName() {
        return name;
    }

    /**
     * Gibt die Teilnehmer des Sportkurses als Stream zurueck.
     *
     * @param teilnamen Bla
     * @param sem       Semester fuer das die Abfrage gewuenscht ist.
     * @return Stream mit Schuelern die an dem Sportkurs teilnehmen.
     */


    public Stream<Schueler> gebeTeilnehmer(Collection<Teilnahme> teilnamen, int sem) {
        return teilnamen.stream()
                .filter(t -> t.istKurs(this))
                .filter(t -> t.istSemester(sem))
                .map(Teilnahme::gebeSchueler);
    }

    /**
     * Gibt die absolute Zahl an Teilnehmern zurueck.
     *
     * @param teilnamen Kommandozeilenparameter
     * @param sem       Semester fuer das die Abfrage gewuenscht ist.
     * @return {@code int} mit der Anzahl an Schuelern.
     */

    public int gebeTeilnehmerZahl(Collection<Teilnahme> teilnamen, int sem) {
        return (int) gebeTeilnehmer(teilnamen, sem).count();
    }

    /**
     * Gibt alle Interesierte Schueler als Stream zuruck.
     * <p>
     * Interesierte Schueler sind die die als Erst oder Zweitwahl den Sportkurs haben
     *
     * @return {@code Stream <Schueler>}
     */

    public Stream<Schueler> gebeInteresierte() {
        return adm.gebeDaten().gebeSchueler().stream().filter(s -> s.wunschtSich(this));
    }

    /**
     * Gibt die absolute Zahl an Interesierten zueruck.
     *
     * @return {@code int}
     */

    public int gebeInteresierteZahl() {
        return (int) gebeInteresierte().count();
    }

    /**
     * Gibt alle Interesierten Schueler als Stream zuruck, welche aber Additum machen.
     *
     * @return {@code Stram <Integer>}
     */

    private Stream<Schueler> gebeAdditumInteressierte() {
        return gebeInteresierte().filter(Schueler::machtAditum);
    }

    /**
     * Gibt die absolute Zahl der Interesierten Schueler zuruck, welche aber Additum machen.
     *
     * @return {@code int}
     */

    public int gebeAdditumInteressierteZahl() {
        return (int) gebeAdditumInteressierte().count();
    }

    /**
     * Allgemeine Methode um zu Pruefen ob Sportkurs einem (oder meheren) Typen entspricht.
     * <p>
     * Dieses wird mit Bitwise and erricht, vereinfacht bswp.:
     * </p>
     <pre>
     ___ Bit ob Zusatzsportart ist oder nicht
     / __ Bit ob Mannschaftssportart ist oder nicht
     |/ _ Bit ob Einzelsportart ist oder nicht
     ||/
     |||  Output | Bedeutung
     Sportkurs Typ:     001  -------|---------------------------------------------------------------------------
     Abgefragter Typ 1: 100  false  | Sportart ist Einzel, will aber wissen ob  es Zusatz ist (1&4==0 --> false)
     Abgefragter Typ 2: 001  true   | Sportart ist Einzel, wie auch Abfrage (1&1!=0 --> true)
     Abgefragter Typ 3: 011  true   | Sportart ist Einzel, und Abfrage will wissen ob
     </pre>
     * <p>
     * Der Vorteil ist das mehere Werte gleichzeitig geprueft werden koennen, das im Arbeitsspeicher weniger (obwohl es
     * vernachlassigbar ist) verbraucht wird, und das der Backup weniger keys und values speichern muss.
     *
     * @param typ Kommandozeilenparameter
     * @return {@code boolean}
     */

    public boolean istTyp(int typ) {
        return (this.typ & typ) != 0;
    }

    /**
     * Gibt an ob Semesster in einem Sportkurs stattfindet.
     * <p>
     * Dieses wird mit dem findetStatt feld und der Abfrage ob mehr Interesierte als Minimal erforderlich sind
     * ermittelt..
     *
     * @param sem Bla
     * @return {@code boolean}
     */

    public boolean findetStatt(int sem) {
        return findetStatt[sem] && gebeInteresierteZahl() > gebeMinimal();
    }

    /**
     * Bestimmt ob Sportkurs in Top "N" ist.
     *
     * @param n   Kommandozeilenparameter
     * @param sem Bla
     * @return {@code boolean}
     */

    public boolean istInTopNKursen(int n, int sem) {
        return istTyp(additumKategNachSem[sem]) ||
                gebeTopNKursen(n - (Sportkurs.gebeAdditumKursZahl(sem)), sem)
                        .anyMatch(k -> k.equals(this));
    }

    /**
     * Ermittelt ob verteilung valide ist.
     * <p>
     * Invalide Verteilung ist wenn in einem Sportkurs additum sein musste, es aber nicht in 2 Semestern (bswp 1 und 2,
     * oder 3 und 4) stattfindet.
     *
     * @return {@code boolean}
     */

    public boolean istVerteilungValide() {
        for (int i = 0; i < findetStatt.length - 1; i += 2)
            if (istTyp(additumKategNachSem[i]) &&
                    !(findetStatt[i] && findetStatt[i])) return false;
        return true;
    }

    /**
     * Bestimmt ob Schueler an einem Sportkurs stattfinden darf. Basiert auf Geschlechtseingrenzungen und Min/Max
     * erforderungen
     *
     * @param s   Kommandozeilenparameter
     * @param t   Bla
     * @param sem Bla
     * @return {@code int}
     */

    public boolean darfTeilnehmen(Schueler s, Collection<Teilnahme> t, int sem) {
        return !((istNurWeiblich() && !s.istWeiblich()) ||
                (istNurMaennlich() && s.istWeiblich()) ||
                (istTyp(ERZWINGE_KAP) && gebeTeilnehmerZahl(t, sem) >= kapazitaet)) ||
                (istTyp(ERZWINGE_MIN) && gebeInteresierteZahl() < minimal);
    }

    /**
     * Befehl einen Sportkurs fuer einen semester zu streichen.
     *
     * @param sem Bla
     */

    public void semesterVerbieten(int sem) {
        darfStattfinden[sem] = true;
    }

    /**
     * Gibt an ob Sportkurs im semster stattfinden darf.
     *
     * @param sem Bla
     * @return {@code boolean}
     */

    public boolean darfStattfinden(int sem) {
        return !darfStattfinden[sem];
    }

    /**
     * Berechnet ob unter umstanden (definiert in der Menge der Teilnahmen) sportkurs in diesem zustand stattfinden
     * koennte.
     *
     * @param t Enthaelt die Teilnahmen die die Schueler gewaehlt haben.
     * @param sem Soll das gewuenschte Semester fuer die Abfrage enthalten.
     * @return Ob stattfinden kann
     */

    public boolean kannStattfinden(Collection<Teilnahme> t, int sem) {
        return (istTyp(ERZWINGE_MIN) && gebeTeilnehmerZahl(t, sem) >= minimal) ||
                (gebeTeilnehmerZahl(t, sem) * Optimierer.PROZEN_UNTE >= minimal);
    }

    /**
     * Alias fuer gebeName()
     *
     * @return Namen
     */

    @Override
    public String toString() {
        return gebeName();
    }
}
