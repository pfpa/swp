package de.hlg.swp.models;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static de.hlg.swp.Main.log;

/**
 * Diese Klasse implementiert einen Schueler. Ein Schuelerobjekt enthaelt die Personendaten, Wuensche, Benutzerdaten, ob
 * er gewaehlt hat und ob eine Ausschlusssportart oder Additum angegeben ist.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Schueler extends Nutzer {
    /** Json Schlussel fuer Backup */
    private static final String NNAME = "n",
            VNAME = "vn",
            PASSWD = "p",
            KLASSE = "k",
            WEIBLICH = "w",
            WUNSCHE = "swp",
            ALTERNAT = "alt",
            ADDIT = "a",
            GEWAHLT = "g",
            AUSSCHLUSS = "aus",
            BNAME = "bn",
            WID = "wi",
            WTYP = "bn";
    /** Enthaelt die notwendigen Daten des jeweiligen Schuelers als JSON-Objekt. */
    final private static Collection<String> jsonVars;

    static {
        jsonVars = new ArrayList<>();
        jsonVars.add(NNAME);
        jsonVars.add(VNAME);
        jsonVars.add(PASSWD);
        jsonVars.add(KLASSE);
        jsonVars.add(WEIBLICH);
        jsonVars.add(WUNSCHE);
        jsonVars.add(ALTERNAT);
        jsonVars.add(ADDIT);
        jsonVars.add(GEWAHLT);
        jsonVars.add(AUSSCHLUSS);
        jsonVars.add(BNAME);
    }

    /** Personendaten: Nachname */
    final private String nachname;
    /** Personendaten: Vorname */
    final private String vorname;
    /** Personendaten: Klasse */
    final private String klasse;
    /** Personendaten: Geschlecht */
    final private boolean weiblich;
    /** Wahl: Wuensche des jeweiligen Schuelers */
    final private List<Wunsch> sportwunsche = new ArrayList<>();
    /** Wahl: Angabe ob der Schueler Additum macht. */
    private boolean additum = false;
    /** Wahl: Angabe ob der Schueler gewaehlt hat. */
    private boolean hatGewahlt = false;
    /** Enthaelt den Benutzernamen. */
    private String user = null;

    /**
     * Konstruktor welcher ein Schuelerobjekt initialisiert mithilfe eines JSON-Objektes.
     * <p>
     * Genutzt fuer Backup regeneration.
     *
     * @param o     JSON-Objekt aus dem Backup
     * @param kurse Kursliste welche fuer die Wahl benutzt wurde
     * @throws Exception Wird geworfen, wenn ein oder mehrere JSON-Keys fehlen.
     */

    public Schueler(JsonObject o, List<Sportkurs> kurse) throws Exception {
        super(o.getString(PASSWD, null));
        if (!o.names().containsAll(jsonVars))
            throw new Exception("missing key/s");
        this.nachname = o.getString(NNAME, null);
        this.vorname = o.getString(VNAME, null);
        this.klasse = o.getString(KLASSE, null);
        this.weiblich = o.getBoolean(WEIBLICH, false);

        for (JsonValue v : o.get(WUNSCHE).asArray())
            sportwunsche.add(new Wunsch(
                    kurse.get(v.asObject().getInt(WID, -1)),
                    v.asObject().getInt(WTYP, -1)));

        this.additum = o.getBoolean(ADDIT, false);
        this.hatGewahlt = o.getBoolean(GEWAHLT, false);
        this.user = o.getString(BNAME, null);
    }

    /**
     * Konstruktor welcher ein Schueler-Objekt mithilfe von Programmdaten initalisiert.
     *
     * @param nachname Nachname des Schuelers
     * @param vorname  Vorname des Schuelers
     * @param klasse   Klasse des Schuelers
     * @param weiblich Geschlecht des Schuelers, wenn weiblich, dann true, bei weiblichem Schueler false.
     */

    public Schueler(String nachname,
                    String vorname,
                    String klasse,
                    boolean weiblich) {
        super();
        this.nachname = nachname;
        this.vorname = vorname;
        this.klasse = klasse;
        this.weiblich = weiblich;
    }

    /**
     * Konvertiert das Objekt in ein JSON-Objekt, fuer Backups
     *
     * @return {@code JSON-Objekt} welches den Schueler beschreibt.
     */

    public JsonObject toJson() {
        JsonArray wunsche = new JsonArray();

        for (Wunsch w : sportwunsche)
            wunsche.add(new JsonObject()
                    .add(WID, w.gebeKurs().gebeID())
                    .add(WTYP, w.gebeTyp()));

        return new JsonObject()
                .add(NNAME, nachname)
                .add(VNAME, vorname)
                .add(PASSWD, gebePasswd())
                .add(KLASSE, klasse)
                .add(WEIBLICH, weiblich)
                .add(WUNSCHE, wunsche)
                .add(ADDIT, additum)
                .add(GEWAHLT, hatGewahlt)
                .add(BNAME, user);
    }

    /**
     * Getter fuer {@link Schueler#sportwunsche}.
     *
     * @return {@code List<Integer>} mit den Sportwuenschen des Schuelers.
     */

    public List<Wunsch> gebeSportwunsche() {
        return sportwunsche;
    }

    /**
     * Updatet die Wahl eines Schuelers und validiert sie teilweise. (Fuer Schueler die kein Additum waehlen.)
     *
     * @param wunsche Liste aller Wunsche
     * @param additum Ob additum
     * @return Wenn true zurueckgegeben wird, dann wurde die Wahl nicht Ordnungsgemaess ausgefuehrt, bei false wird die
     * Wahl angenommen.
     */

    public boolean updateInfo(Collection<Wunsch> wunsche, boolean additum) {
        Sportkurs auschluss = wunsche.stream()
                .filter(w -> w.istTyp(Wunsch.NICHTWAHL))
                .map(Wunsch::gebeKurs)
                .findFirst().orElse(null);

        for (Wunsch w : wunsche)
            if (!w.istTyp(Wunsch.NICHTWAHL) && auschluss != null && w.istKurs(auschluss) ||
                    wunsche.stream().filter(W -> W.istKurs(w.gebeKurs())).count() >= 3 ||
                    w.gebeKurs().istNurMaennlich() && istWeiblich() ||
                    w.gebeKurs().istNurWeiblich() && istMannlich()) return true;

        if (wunsche.stream().filter(w -> w.gebeKurs().istTyp(Sportkurs.EINZEL)).count() < 1 ||
                wunsche.stream().filter(w -> w.gebeKurs().istTyp(Sportkurs.EINZEL)).count() < 1) return true;

        sportwunsche.clear();
        sportwunsche.addAll(wunsche);

        log.info("Schueler " + gebeUsername() + " wunscht sich: " + wunsche.stream()
                .map(w -> String.format("%s [%s], ",
                        w.gebeKurs().gebeName(),
                        w.istTyp(Wunsch.ERSTWAHL) ? "E" : w.istTyp(Wunsch.NICHTWAHL) ? "N" : "A"))
                .reduce("", String::concat));
        this.additum = additum;
        hatGewahlt = true;
        return false;
    }

    /**
     * Getter fuer {@link Schueler#hatGewahlt}.
     *
     * @return {@code boolean}
     */

    public boolean hatGewahlt() {
        return hatGewahlt;
    }

    /**
     * Getter fuer {@link Schueler#nachname}.
     *
     * @return {@code String}
     */

    public String gebeNachnamen() {
        return nachname;
    }

    /**
     * Getter fuer {@link Schueler#vorname}.
     *
     * @return {@code String}
     */

    public String gebeVornamen() {
        return vorname;
    }

    /**
     * Getter fuer {@link Schueler#klasse}.
     *
     * @return {@code String}
     */

    public String gebeKlasse() {
        return klasse;
    }

    /**
     * Getter fuer {@link Schueler#weiblich}.
     *
     * @return {@code boolean}
     */

    public boolean istWeiblich() {
        return weiblich;
    }

    /**
     * Negierter Getter fuer {@link Schueler#weiblich}. (Fuer Maenner)
     *
     * @return {@code boolean}
     */

    public boolean istMannlich() {
        return !weiblich;
    }

    /**
     * Getter fuer {@link Schueler#additum}.
     *
     * @return {@code boolean}
     */

    public boolean machtAditum() {
        return hatGewahlt && additum;
    }

    /**
     * Generator und Getter fuer {@link Schueler#user}.
     *
     * @param namen Alle Namen in der Schuelerliste.
     * @return Generierter Benutzername der aus 4 oder mehr Buchstaben des Vor- und 4 oder mehr Buchstaben des
     * Nachnamens besteht.
     */

    public String gebeUsername(Set<String> namen) {
        if (user != null) return user;

        String name;
        for (int i = 0; /* so lange wie noetig */ ; i++) {
            name = nachname.subSequence(0, Math.min(nachname.length(), 4 + i)).toString();
            name += vorname.subSequence(0, Math.min(vorname.length(), 4 + i));

            if (i > vorname.length() && i > nachname.length())
                name += "_" + (i - Math.max(vorname.length(), nachname.length()));

            name = name.toLowerCase().trim().replaceAll("\\s", "_");
            if (namen == null || !namen.contains(name))
                return user = name;
        }
    }

    /**
     * Getter fuer {@link Schueler#user}
     *
     * @return Benutzername des Schuelers.
     */

    public String gebeUsername() {
        return user;
    }

    /**
     * Methode die dem Programm ob der Schueler den gegebenen Kurs k gewaehlt hat.
     *
     * @param k Abgefragter Sportkurs
     * @return true wenn enthalten in den Wuenschen, false wenn nicht.
     */

    public boolean wunschtSich(Sportkurs k) {
        return sportwunsche.stream()
                .anyMatch(w -> w.istKurs(k));
    }

    /**
     * Gibt an wie haufig der Schueler an einem Kurs teilnimmt.
     * <p>
     * Dieses wird mithillfe der bisherigen Teilnamen bestimmt.
     *
     * @param kurs      Bla
     * @param teilnamen Bla
     * @return int
     */

    public long gebeTeilnahmeHaufigkeit(Sportkurs kurs, Collection<Teilnahme> teilnamen) {
        return teilnamen.stream()
                .filter(t -> t.istSchueler(this))
                .filter(t -> t.istKurs(kurs))
                .count();
    }

    /**
     * Gibt an wie haufig der Schueler an einem Sporttypen teilnimmt.
     * <p>
     * Dieses wird mithillfe der bisherigen Teilnahmen bestimmt.
     *
     * @param typ       Bla
     * @param teilnamen Bla
     * @return int
     */

    public long gebeTypTeilnahmeHaufigkeit(int typ, Collection<Teilnahme> teilnamen) {
        return teilnamen.stream()
                .filter(t -> t.istSchueler(this))
                .filter(t -> t.gebeKurs().istTyp(typ))
                .count();
    }

    /**
     * Gibt an ob schueler diesen Sportkurs umbedingt besetzen muss
     *
     * @param k Sportkurs
     * @param t Teilnahmen Menge
     * @return ob Besetzungserwingung vorliegt
     */

    public boolean mussKursBesetzen(Sportkurs k, Collection<Teilnahme> t) {
        return (k.istTyp(Sportkurs.EINZEL) &&
                gebeTypTeilnahmeHaufigkeit(Sportkurs.EINZEL, t) <= 1) ||
                (k.istTyp(Sportkurs.MANNSCHAFT) &&
                        gebeTypTeilnahmeHaufigkeit(Sportkurs.MANNSCHAFT, t) <= 1);
    }

    /**
     * Gibt namen einer Erstwahl nach index
     *
     * @param i index
     * @return name des Kurses
     */

    public String gebeErstwahlNachIndex(int i) {
        return sportwunsche.stream()
                .filter(w -> w.istTyp(Wunsch.ERSTWAHL))
                .skip(i)
                .map(Wunsch::gebeKurs)
                .map(Sportkurs::gebeName)
                .findFirst().orElse("-n/a-");
    }

    /**
     * Gibt namen einer Alt-wahl nach index
     *
     * @param i index
     * @return name des Kurses
     */

    public String gebeAltwahlNachIndex(int i) {
        return sportwunsche.stream()
                .filter(w -> w.istTyp(Wunsch.ALTWAHL))
                .skip(i)
                .map(Wunsch::gebeKurs)
                .map(Sportkurs::gebeName)
                .findFirst().orElse("-n/a-");
    }

    /**
     * Gibt namen einer nichtsportart
     *
     * @return name des Kurses
     */

    public String gebeNichtsport() {
        return sportwunsche.stream()
                .filter(w -> w.istTyp(Wunsch.NICHTWAHL))
                .map(Wunsch::gebeKurs)
                .map(Sportkurs::gebeName)
                .findFirst().orElse("-n/a-");
    }

    /**
     * Sucht den Namen eines Kurses, welches vom
     * Schueler in einem Semeter belegt wird
     * parallel aus.
     *
     * @param sem Semester im Kontext
     * @param t   Teilnahmen menge
     * @return Gibt die Teilnahmen eines Schuelers nach Semster aus.
     */

    public String gebeTeilnahmeNachSemester(int sem, Collection<Teilnahme> t) {
        return t.stream().parallel()
                .filter(T -> T.istSchueler(this))
                .filter(T -> T.istSemester(sem))
                .map(T -> T.gebeKurs().gebeName())
                .findAny().orElse("-n/a-");
    }
}
