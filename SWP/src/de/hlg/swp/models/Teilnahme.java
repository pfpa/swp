package de.hlg.swp.models;

/**
 * Klasse die eine Teilnahme eines Schuelers in einem Kurs fuer ein Semster dastellt darstellt.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */
public class Teilnahme {

    /** Semester der Teilnahme */
    final private int semester;
    /** Schueler welcher den angegebenen Kurs im angegebenen Semester gewaehlt hat. */
    final private Schueler schueler;
    /** Kurs welcher gewaehlt wurde. */
    private Sportkurs kurs;

    /**
     * Konstruktor welcher ein Teilnahmeobjekt initalisiert.
     *
     * @param k Sportkurs der gewaehlt wurde.
     * @param s Schueler der gewaehlt hat.
     * @param h Semester fuer das gewaehlt wurde.
     */

    public Teilnahme(Sportkurs k, Schueler s, int h) {
        this.kurs = k;
        this.schueler = s;
        this.semester = h;
    }

    /**
     * Getter fuer {@link Teilnahme#schueler}.
     *
     * @return {@code Schueler} ist der Rueckgabetyp.
     */

    public Schueler gebeSchueler() {
        return schueler;
    }

    /**
     * Getter fuer {@link Teilnahme#kurs}.
     *
     * @return {@code Sportkurs} ist der Rueckgabetyp
     */

    public Sportkurs gebeKurs() {
        return kurs;
    }

    /**
     * Setter fuer {@link Teilnahme#kurs}
     *
     * @param kurs Neuer Kurs
     */

    public void setKurs(Sportkurs kurs) {
        this.kurs = kurs;
    }

    /**
     * Getter fuer {@link Teilnahme#semester}.
     *
     * @return {@code int} ist der Rueckgabetyp.
     */

    public int gebeSemester() {
        return semester;
    }

    public boolean istSemester(int sem) {
        return semester == sem;
    }

    public boolean istKurs(Sportkurs k) {
        return kurs.equals(k);
    }

    public boolean istSchueler(Schueler s) {
        return schueler.equals(s);
    }
}
