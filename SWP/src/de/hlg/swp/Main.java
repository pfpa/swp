package de.hlg.swp;

import de.hlg.swp.models.Nutzer;
import de.hlg.swp.models.Sportkurs;
import de.hlg.swp.server.Admin;
import de.hlg.swp.utils.Backup;
import de.hlg.swp.utils.Conf;

import java.util.logging.Logger;

/**
 * Main-Klasse die fuer die Steuerung des Programms zustaendig ist. Sie implementiert auch die Backup-Funktion.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Main {
    /** Statische Variable die dafuer zustaendig ist die Reihenfolge der Kurse fuer Additumler zu bestimmen. */
    public final static int[] additumKategNachSem = new int[]{
            Sportkurs.EINZEL,
            Sportkurs.EINZEL,
            Sportkurs.MANNSCHAFT,
            Sportkurs.MANNSCHAFT
    };
    /** Log-Objekt welches den Log aufzeichnet. */
    final public static Logger log = Logger.getGlobal();
    /** Admin-Objekt, welches die Admin-Oberflaeche steuert und den Schuelerserver starten kann. */
    public static Admin adm;
    /** Backup-Objekt, welches nur fuer das durchfuehren der Backups zustaendig ist. */
    public static Backup bcp;
    /** Objekt mit den Standardeinstellungen. */
    public static Conf conf;

    /**
     * Einsteigspunkt mit laden der Standardeinstellungen (siehe: {@link de.hlg.swp.utils.Conf}), Initalisierung der
     * Standardeinstellungen (siehe: {@link de.hlg.swp.utils.Conf}) und Passworteinlesen mithilfe von {@link
     * de.hlg.swp.models.Nutzer}
     *
     * @param args Kommandozeilenparameter welche moeglich sind: -D; -p; -b; -t; -r; -h
     */

    public static void main(String[] args) {
        conf = new Conf(args);
        bcp = new Backup();

        Nutzer.configuriereWorter();

        if (conf.RECOV != null) {
            bcp.stelleBackupWiederHer(conf.RECOV);
            adm = new Admin(bcp);
        } else adm = new Admin();
    }

}
