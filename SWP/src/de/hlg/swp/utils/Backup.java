package de.hlg.swp.utils;

import com.eclipsesource.json.*;
import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Sportkurs;
import de.hlg.swp.server.Admin;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static de.hlg.swp.Main.*;

/**
 * Klasse die fuer die Implementierung der Backupfunktion zustaendig.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Backup {

    /** Backup-Schluessel fuer den Backup. */
    private static final String KURSE = "k",
            SCHUELER = "s",
            STATUS = "st";

    /** Zeitstempel fuer den Backup-Dateinamen. */
    private static final SimpleDateFormat stamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    /** Liste aller Kurse fuer Backup regeneration */
    final public List<Sportkurs> kurse = new ArrayList<>();
    /** Menge aller Schueler fuer Backup regeneration */
    public Set<Schueler> schueler = new HashSet<>();
    /** Status fuer die Anzeige im Admin-Bereich des Browsers */
    public int status = -1;

    /**
     * Backup-Konstruktor der die Auto-Backup-Funktion in einem neuen Thread startet, und beim Herunterfahren ein Backup
     * generiert
     */

    public Backup() {
        new BackupThread().start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                if (adm != null && adm.getStatus() != Admin.FERTIG)
                    fuehreBackupDurch();
            }
        });

        log.config(String.format("Backups in \"%s\" alle %d minuten.", conf.BACKUP, conf.BACKUP_INTERVAL));
    }

    /**
     * Backupfunktion, welche die Daten im JSON-Format speichert.
     */

    public void fuehreBackupDurch() {
        if (adm == null || adm.gebeDaten() == null)
            return;

        if (!conf.BACKUP.isDirectory())
            conf.BACKUP.delete();
        if (!conf.BACKUP.exists())
            conf.BACKUP.mkdir();

        JsonObject o = new JsonObject();
        o.add(STATUS, adm.getStatus());

        JsonArray kur = new JsonArray(),
                scu = new JsonArray();

        adm.gebeDaten().gebeKurse().stream()
                .filter(Sportkurs::istVerteilungValide)
                .forEach(k -> kur.add(k.toJson()));
        o.add(KURSE, kur);

        adm.gebeDaten().gebeSchueler().stream()
                .forEach(s -> scu.add(s.toJson()));
        o.add(SCHUELER, scu);

        try {
            FileWriter fw = new FileWriter(conf.BACKUP + File.separator + stamp.format(new Date()) + ".bc");
            o.writeTo(fw, conf.DEBUG ? WriterConfig.PRETTY_PRINT : WriterConfig.MINIMAL);
            fw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Backupfunktion welche die Backups wiederherstellt nach Absturz o.ae.
     *
     * @param f Dateiname
     */
    public void stelleBackupWiederHer(File f) {

        try {
            JsonObject o = Json.parse(new FileReader(f)).asObject();


            if (o.names().contains(KURSE)) {
                try {
                    for (JsonValue v : o.get(KURSE).asArray())
                        kurse.add(new Sportkurs(v.asObject()));
                } catch (Exception e) {
                    e.printStackTrace();
                    log.severe("Backup Datei hat nicht alle Felder fur Kurse.");
                    System.exit(1);
                }
                log.info("Backup stellte " + kurse.size() + " kurse wieder her.");
            } else {
                log.warning(" Backup datei ohne kurse. Ignoriert.");
            }

            if (o.names().contains(SCHUELER)) {
                try {
                    for (JsonValue v : o.get(SCHUELER).asArray())
                        schueler.add(new Schueler(v.asObject(), kurse));
                } catch (Exception e) {
                    e.printStackTrace();
                    log.severe("Backup Datei hat nicht alle Felder fur Schueler.");
                    System.exit(1);
                }
                log.info("Backup stellte " + schueler.size() + " Schueler wieder her.");
            } else {
                log.warning("Backup Datei ohne Schueler. Ignoriert.");
            }

            status = o.getInt(STATUS, -1);
            log.info("Backup stellte status " + status + " wieder her.");
        } catch (IOException ioe) {
            log.severe("Fehler beim lesen der Backupdatei: " + ioe.getMessage());
            System.exit(1);
        }
    }

    /**
     * Diese statische Klasse implementiert Backup in einem eigenen Thread.
     *
     * @author HLG - 1Inf1 - Jahrgang: 15/16
     * @version 1.0
     */

    private static class BackupThread extends Thread {

        /**
         * Diese Methode fuehrt die Backupfunktion alle Stunde aus (Annahme: {@code BACKUP_INTERVAL == 60})
         */

        @Override
        public void run() {
            try {
                for (; ; ) {
                    Thread.sleep(1000 * 60 * conf.BACKUP_INTERVAL);
                    long start = System.currentTimeMillis();
                    bcp.fuehreBackupDurch();
                    log.info("backup in " + (System.currentTimeMillis() - start) + " ms");
                    if (interrupted()) break;
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}
