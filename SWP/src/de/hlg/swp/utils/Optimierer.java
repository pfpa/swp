package de.hlg.swp.utils;

import de.hlg.swp.Main;
import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Sportkurs;
import de.hlg.swp.models.Teilnahme;
import de.hlg.swp.models.Wunsch;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import static de.hlg.swp.Main.*;

/**
 * Optimizierungs Klasse.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Optimierer {

    /** Interne Faktoren welche im Optimizations-Algorithmus verwendet werden */
    static public double
            START_IDEAL = 1,
            START_ALTER = 25,
            START_ANSON = 5000,
            START_NICHT = Double.POSITIVE_INFINITY,
            BASIS_ART = 5,
            BASIS_TYP = 4,
            PROZEN_UEBE = 1.2,
            PROZEN_UNTE = 0.8;
    /**
     * Alle Schueler fuer die Simulation
     */
    final private Set<Schueler> schueler;
    /** Alle Schueler fuer die Simulation */
    final private Set<Sportkurs> kurse;
    /**
     * Definiert output vom Optimiz log (bei conf.DEBUG, stdout, an sonsten in "optimiz.log"
     */
    private PrintStream l;
    /** Der am Ende ausgegebene Output (wer [schueler], wo [kurs], wann [semester]) */
    private Set<Teilnahme> teilnahmen = new HashSet<>();

    /**
     * Konstrukor. Benoetigt eine Nicht leere schueler und kurs liste, die Intern Shallow-koppiert werden, damit
     * hypothethisch Optimiz mehrfach parallel laufen kann.
     *
     * @param schueler Alle Schueler
     * @param kurse    Alle Kurse
     */

    public Optimierer(Collection<Schueler> schueler,
                      Collection<Sportkurs> kurse) {
        if (schueler.size() == 0)
            throw new IllegalArgumentException("Schueler liste leer.");
        if (kurse.size() == 0)
            throw new IllegalArgumentException("Sportkurs liste leer.");


        this.schueler = new HashSet<>(schueler);
        this.kurse = new HashSet<>(kurse);

        try {
            l = Main.conf.DEBUG ? System.out : new PrintStream(new FileOutputStream("optimiz.log"));
        } catch (IOException ioe) {
            (l = System.out).printf("Konnte optimiz log nicht in Datei schreiben (%s), default auf stdout.\n", ioe.getMessage());
        }

        log.info("Anfang der Optimization...");
        long start = System.nanoTime();
        for (int sem = 0; sem < additumKategNachSem.length; sem++)
            verarbeite(sem);

        l.print("Sportkurs\t\t");
        for (int sem = 0; sem < additumKategNachSem.length; sem++)
            l.printf("Sem %d\t\t", sem+1);
        l.println();

        kurse.forEach(k -> {
            l.printf("%-8s", k.gebeName().subSequence(0, Math.min(8, k.gebeName().length())));
            IntStream.range(0, additumKategNachSem.length).forEach(sem ->
                    l.printf("\t\t%-2d (%d)",
                            teilnahmen.stream()
                                    .filter(t -> t.istSemester(sem))
                                    .filter(t -> t.istKurs(k))
                                    .count(),
                            teilnahmen.stream()
                                    .filter(t -> t.istSemester(sem))
                                    .filter(t -> t.istKurs(k))
                                    .filter(t -> t.gebeSchueler().machtAditum())
                                    .count()));
            l.print("\n");
        });

        l.format("Additum: %d; Wahler: %d; Gesamt: %d\n",
                schueler.stream().filter(Schueler::machtAditum).count(),
                schueler.stream().filter(Schueler::hatGewahlt).count(),
                schueler.size());

        log.info("Ende der Optimization in " + (System.nanoTime() - start) / 1e6 + "ms");
    }

    /**
     * Traegt .gebeSchueler() fuer Semester in Kurs ein. Geneiert auch log infos
     *
     * @param sch Schueler
     * @param kur Kurs
     * @param sem Semester
     */

    private void trageEin(Schueler sch, Sportkurs kur, int sem) {
        if (kur == null) return;
        l.printf("+ %9s [%s] in %s\t(sem %d, zufr %e) [%s]\n",
                sch.gebeUsername(),
                sch.machtAditum() ? "A" :
                        (sch.hatGewahlt() ? "W" : "N"),
                kur.gebeName(), sem + 1,
                zufr(sch, kur, sem),
                sch.gebeSportwunsche().stream()
                        .filter(w -> w.istKurs(kur))
                        .map(w -> w.istTyp(Wunsch.ERSTWAHL) ?
                                "E" : w.istTyp(Wunsch.ALTWAHL) ?
                                "A" : w.istTyp(Wunsch.NICHTWAHL) ?
                                "N" : "X")
                        .findAny().orElse("X"));
        teilnahmen.add(new Teilnahme(kur, sch, sem));
    }

    /**
     * Berechnet fuer einen Schueler welcher Kurs in einem Semester geeignet waere, unter berucksichtigung von Regeln
     * (nur eintragen wenn ueberhaup moeglich) und theorethischen Schueler Altruismus.
     *
     * @param s   Schueler
     * @param sem Semester
     * @return Resultierendere Kurs
     */

    private Sportkurs gebeGeignetenKurs(Schueler s, int sem) {
        return kurse.stream()
                .filter(k -> k.findetStatt(sem))
                .filter(k -> k.istInTopNKursen(conf.MAX_KURS_ZAHL, sem))
                .filter(k -> k.darfTeilnehmen(s, teilnahmen, sem))
                .filter(k -> k.darfStattfinden(sem))
                .filter(k -> !(k.istTyp(Sportkurs.ZUSATZ) && s.gebeTypTeilnahmeHaufigkeit(Sportkurs.ZUSATZ, teilnahmen) != 0))
                .filter(k -> !((s.gebeTypTeilnahmeHaufigkeit(Sportkurs.EINZEL, teilnahmen) > 2 && !k.istTyp(Sportkurs.EINZEL)) ||
                        (s.gebeTypTeilnahmeHaufigkeit(Sportkurs.MANNSCHAFT, teilnahmen) > 2 && !k.istTyp(Sportkurs.MANNSCHAFT))))
                .min(Comparator.comparing(k -> k.gebeTeilnehmerZahl(teilnahmen, sem))).orElse(null);
    }

    /**
     * Versucht, mit kleinstmoeglichen Schaden, einen Kurs zum stattfinden zum bringen, wobei es diejenigen
     * umzufriedensten schueler aus anderen kursen in diesen kurs holt, bis dieses die minimale Anzahl an teilnehmern
     * hat.
     * <p>
     * Gegenteil von verteileKurs.
     *
     * @param k   Kurs zum aufloesen
     * @param sem Semester in dem Kurs aufgeloest wird
     */

    private void erzwingeStattfinden(Sportkurs k, int sem) {
        l.printf("%% kurs %s wird in Semester %d erzwungen...\n", k.gebeName(), sem);
        teilnahmen.stream().parallel()
                .filter(t -> t.istSemester(sem))
                .filter(t -> !t.istKurs(k))
                .filter(t -> t.gebeKurs().gebeMinimal() < t.gebeKurs().gebeTeilnehmerZahl(teilnahmen, sem))
                .filter(t -> !t.gebeSchueler().mussKursBesetzen(t.gebeKurs(), teilnahmen))
                .sorted(Comparator.comparing(t -> zufr(t.gebeSchueler(), t.gebeKurs(), sem)))
                .limit(k.gebeMinimal() - k.gebeTeilnehmerZahl(teilnahmen, sem))
                .forEach(t -> {
                    l.printf("~ %s aus %s\t(sem %d) ==> %s\n",
                            t.gebeSchueler().gebeUsername(),
                            t.gebeKurs().gebeName(),
                            sem,
                            k.gebeName());
                    t.setKurs(k);
                });
    }

    /**
     * Verteilt Schueler der Simulation aus einem Semester, welche an einem Kurs teilnehmen auf, weil dieses nun
     * aufgeloest wird.
     * <p>
     * Gegenteil von erzwingeStattfinden.
     *
     * @param k   Kurs zum aufloesen
     * @param sem Semester in dem Kurs aufgeloest wird
     */

    private void verteileKurs(Sportkurs k, int sem) {
        l.printf("* kurs %s wird in Semester %d aufgelosst...\n", k.gebeName(), sem);
        k.semesterVerbieten(sem);
        teilnahmen.stream()
                .filter(t -> t.istKurs(k))
                .filter(t -> t.istSemester(sem))
                .forEach(t -> {
                    if (t.gebeSchueler().machtAditum())
                        return;
                    l.printf("- %s aus %s\t(sem %d) ==> ",
                            t.gebeSchueler().gebeUsername(), t.gebeKurs().gebeName(), sem);
                    t.setKurs(t.gebeSchueler().hatGewahlt() ?
                            kurse.stream()
                                    .filter(K -> K.darfTeilnehmen(t.gebeSchueler(), teilnahmen, sem))
                                    .filter(K -> !k.equals(K))
                                    .min(Comparator.comparing(K -> zufr(t.gebeSchueler(), K, sem)))
                                    .orElseGet(null) :
                            gebeGeignetenKurs(t.gebeSchueler(), sem));
                    l.printf("zu %s\n", t.gebeKurs().gebeName());
                });
        l.printf("! kurs %s mit folgenden Schuelern gelassen: %s",
                k.gebeName(),
                k.gebeTeilnehmer(teilnahmen, sem)
                        .map(s -> s.gebeUsername() + " ")
                        .reduce("", String::concat));
    }

    /**
     * Verarbeitet einen Semester. Schueler werden erst in zufallige Reihenfolge gebracht, dadurch das schueler ein
     * HashSet ist.
     * <p>
     * Danach wir jeder Additumsteilnehmer (welcher auch wahler ist), seinem Wunschkurs fur das Semester, (erstmal)
     * beidingungslos zugeteilt.
     * <p>
     * Darauf werden alle Wahler (welche kein Additum machen), in denjenigen Kurs eingetragen, wo die ego/altuistische
     * Zufriedenheitsfunktion zufr die Hoechte zufriedenheit berechnet (d.h. kleinst Zahl)
     * <p>
     * Als vorletztes werden alle Nichtwahler als "Buffer" in diejenigen Kurs zugewiesen welche {@code
     * gebeGeignetenKurs} als am besten passendes empfindet.
     * <p>
     * Zuletzt werden alle Kurse nochmal Kontroliert, und sichergestellt das alle die nicht stattfinden durfen
     * (unterbestazung) aufgeloest werden, und nach {@code verteileKurs} umverteilt werden. Kurse welche stattfinden
     * muessen (Additum) werden versucht mit kleinstmoeglichen Schaden zum Stattfinden zu Provozieren.
     *
     * @param sem Kommandozeilenparameter
     */

    private void verarbeite(int sem) {
        schueler.stream()
                .filter(Schueler::machtAditum)
                .forEach(s -> trageEin(s,
                        ((Main.additumKategNachSem[sem] == Sportkurs.EINZEL) ?
                                s.gebeSportwunsche().get(0).gebeKurs() :
                                s.gebeSportwunsche().get(1).gebeKurs()), sem));

        schueler.stream()
                .filter(Schueler::hatGewahlt)
                .filter(s -> !s.machtAditum())
                .forEach(s -> trageEin(s, kurse.stream().min(Comparator.comparing(k -> zufr(s, k, sem))).orElse(null), sem));

        schueler.stream()
                .filter(s -> !s.hatGewahlt())
                .forEach(s -> trageEin(s, gebeGeignetenKurs(s, sem), sem));

        kurse.stream()
                .filter(k -> !(k.istTyp(Sportkurs.ERZWINGE_MIN) && (k.gebeTeilnehmerZahl(teilnahmen, sem) >= k.gebeMinimal())))
                .filter(k -> k.gebeTeilnehmerZahl(teilnahmen, sem) < k.gebeMinimal() * PROZEN_UNTE)
                .filter(k -> !k.istTyp(additumKategNachSem[sem]))
                .filter(k -> k.gebeTeilnehmerZahl(teilnahmen, sem) > 0)
                .sorted(Comparator.comparing(k -> -k.gebeTeilnehmerZahl(teilnahmen, sem)))
                .forEach(k -> verteileKurs(k, sem));

        kurse.stream()
                .filter(k -> k.istTyp(additumKategNachSem[sem]))
                .filter(k -> !k.kannStattfinden(teilnahmen, sem))
                .forEach(k -> erzwingeStattfinden(k, sem));
    }

    /**
     * Zufriedenheitsfunktion. Gibt eine Zahl (kleiner ist besser), welche die zufiednheit eines SCHUELERS mit einem
     * KURS in einem SEMESTER angibt.
     * <p>
     * Vorherige teilnamen werden aus dem {@code teilnahmen} Set abgelesen.
     *
     * @param s   Ein Schueler
     * @param k   Ein Sportkurs
     * @param sem Semester
     * @return Der Wert der Zufriedenheit, umso naeher er an 0 ist, desto besser.
     */

    private double zufr(Schueler s, Sportkurs k, int sem) {
        if (!k.findetStatt(sem) ||
                !k.istInTopNKursen(conf.MAX_KURS_ZAHL, sem) ||
                !k.darfTeilnehmen(s, teilnahmen, sem) ||
                !k.darfStattfinden(sem) ||
                k.gebeTeilnehmerZahl(teilnahmen, sem) / k.gebeKapazitaet() > PROZEN_UEBE ||
                s.gebeTypTeilnahmeHaufigkeit(k.gebeTyp() & Sportkurs.ALLE, teilnahmen) > 2 ||
                k.istTyp(Sportkurs.ZUSATZ) && s.gebeTypTeilnahmeHaufigkeit(k.gebeTyp() & Sportkurs.ALLE, teilnahmen) > 1)
            return Double.POSITIVE_INFINITY;

        return s.gebeSportwunsche().stream()
                .filter(w -> w.istKurs(k))
                .map(w -> w.istTyp(Wunsch.ERSTWAHL) ?
                        START_IDEAL : w.istTyp(Wunsch.ALTWAHL) ?
                        START_ALTER : w.istTyp(Wunsch.NICHTWAHL) ?
                        START_NICHT : START_ANSON)
                .findAny().orElse(START_ANSON)
                * (Math.pow(BASIS_ART, s.gebeTeilnahmeHaufigkeit(k, teilnahmen) + 1))
                * (Math.pow(BASIS_TYP, s.gebeTypTeilnahmeHaufigkeit(k.gebeTyp(), teilnahmen) + 1))
                * (Math.pow(k.gebeTeilnehmerZahl(teilnahmen, sem), 2) + 1)
                / (k.gebeMinimal() + 1)
                / (k.gebeKapazitaet() + 1);
    }

    /**
     * Gibt die Resultate der Optimiertung wieder her.
     *
     * @return {@code List<Teilnahme>}
     */

    public Collection<Teilnahme> gebeResultate() {
        return teilnahmen;
    }
}
