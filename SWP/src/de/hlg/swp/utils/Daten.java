package de.hlg.swp.utils;

import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Sportkurs;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Klasse zum verwalten der Daten waehrend der Laufzeit des Programms.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Daten {

    /** Kursmenge mit den Kursen die angeboten werden. */
    private final List<Sportkurs> kurse = Collections.synchronizedList(new ArrayList<>());
    /** Map in der alle Schueler gespeichert mit Benutzernamen als Schlussel. */
    private final Map<String, Schueler> schueler = new ConcurrentHashMap<>();

    /**
     * Konstuktor der mit den Argumenten die Schuelerliste und die Sportkurse ins Programm ueberfuehrt.
     *
     * @param schueler Collection welche die Schueler mit allen Datenfeldern enthalten muss.
     * @param kurse    Liste in der alle Kurse stehen die angeboten werden.
     */

    public Daten(Set<Schueler> schueler, List<Sportkurs> kurse) {
        schueler.forEach(s -> this.schueler.put(s.gebeUsername(this.schueler.keySet()), s));
        kurse.forEach(this.kurse::add);
    }

    /**
     * Getter fuer {@link Daten#schueler}
     *
     * @return {@code Collection<Schueler>} enthaelt alle Schuelerobjekte.
     */

    public Set<Schueler> gebeSchueler() {
        return new HashSet<>(schueler.values());
    }

    /**
     * Getter fuer {@link Daten#kurse}
     *
     * @return {@code Set <Sportkurs>} mit den Kursen.
     */

    public List<Sportkurs> gebeKurse() {
        return kurse;
    }

    /**
     * Gibt zu einem Benutzernamen den Schueler.
     *
     * @param k Benutzername des gesuchten Schuelers.
     * @return {@code Schueler} ist das zurueckgegebene Objekt.
     */

    public Schueler UserToSchueler(String k) {
        return schueler.get(k);
    }
}
