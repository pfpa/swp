package de.hlg.swp.utils;

import java.io.File;

import static de.hlg.swp.Main.log;

/**
 * Klasse welche die Standardeinstellungen enthaehlt und die ausfuehrbaren Modi des Programms steuert und enthaelt.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Conf {
    /** Variable die bestimmt ob das Programm im Debug-Modus startet/laeuft. */
    public boolean DEBUG = false;
    /** Ordner in dem die Backups liegen. */
    public File BACKUP = new File("backups");
    /** Backup-Interval in Minuten */
    public int BACKUP_INTERVAL = 60;
    /** Port auf dem der Schuelerserver gestartet wird. */
    public int PORT = 8080;
    /** Die Passwort-Schwierigkeit. (Ist effektiv nur die Anzahl der Woerter die verkettet werden.) */
    public int PASSWD_DIFF = 3;
    /** Maximale Kursanzahl die stattfinden soll. (wird  auf der Admin-Oberflaeche eingestellt.) */
    public int MAX_KURS_ZAHL = 5;
    /** Datei die als Argument beim starten via Backup benoetigt wird. */
    public File RECOV = null;
    /**
     * Datei aus welchen die passworter generiert werden (optional)
     */
    public File PASSWD_FILE = null;

    /**
     * Konstruktor der das Conf- Objekt initalisiert.
     *
     * @param args Programmparameter die beim starten angegeben werden.
     */

    public Conf(String[] args) {
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-D":
                    DEBUG = true;
                    log.info("In DEBUG mode gestartet");
                    break;
                case "-p":
                    try {
                        if (++i < args.length) PORT = Integer.parseInt(args[i]);
                        else {
                            log.severe("-p flag braucht port als Argument!");
                            System.exit(1);
                        }
                    } catch (NumberFormatException nfe) {
                        log.severe("-p: " + args[i] + " ist keine gultige Zahl!\n");
                        System.exit(1);
                    }
                    break;
                case "-b":
                    if (++i < args.length) BACKUP = new File(args[i]);
                    else log.warning("-b ohne argument, nimmt defaut an: " + BACKUP);
                    break;
                case "-t":
                    try {
                        if (++i < args.length) BACKUP_INTERVAL = Integer.parseInt(args[i]);
                        else {
                            log.severe("-t flag braucht stunden als Argument!");
                            System.exit(1);
                        }
                    } catch (NumberFormatException nfe) {
                        log.severe("-t: " + args[i] + " ist keine gultige Zahl!\n");
                        System.exit(1);
                    }
                    break;
                case "-r":
                    if (++i < args.length) RECOV = new File(args[i]);
                    else {
                        log.severe("-r flag braucht Dateinamen als argument!");
                        System.exit(1);
                    }
                    break;
                case "-P":
                    if (++i < args.length) PASSWD_FILE = new File(args[i]);
                    else {
                        log.severe("-P flag braucht Dateinamen als argument!");
                        System.exit(1);
                    }
                    break;
                case "-d":
                    try {
                        if (++i < args.length) PASSWD_DIFF = Integer.parseUnsignedInt(args[i]);
                        else {
                            log.severe("-d flag braucht Zahl als Argument!");
                            System.exit(1);
                        }
                    } catch (NumberFormatException nfe) {
                        log.severe("[err] -d: " + args[i] + " ist keine gultige Zahl!\n");
                        System.exit(1);
                    }
                    break;
                case "-h":
                    System.err.print("SWP - Sport Wahl Project\n" +
                            "Nutzung: SWP.jar [optionen]\n\n" +
                            "Optionen:\n" +
                            "\t-D:\t\tDebugging. Admin port auf 4000 und keine Passwortkontrolle\n" +
                            "\t-p [num]:\tPort: Festsetzen des Schuelerserver HTTP Ports\n" +
                            "\t-b [file]:\tBackup Ordner name [default: " + BACKUP + "]\n" +
                            "\t-t [num]:\tBackup thread mit Interval in Minuten\n" +
                            "\t-r [file]:\tRecover backup mit ausgewahlter Datei.\n" +
                            "\t-P [file]:\tDatei aus denen Zufallspassworter generiert werden.\n" +
                            "\t-d:\t\tStellt Passwort schwierigkeit ein (3 [worter] default).\n" +
                            "\t-h:\t\tHelp: Dieser Text\n");
                    System.exit(0);
                    break;
                default:
                    log.severe("Invalide Option \"" + args[i] + "\"\n");
                    System.exit(1);
            }
        }
    }
}
