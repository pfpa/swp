package de.hlg.swp.utils;

import de.hlg.swp.models.Schueler;
import de.hlg.swp.models.Sportkurs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Dateileser die fuer das Programm gebraucht werden. Umfasst das Format .csv fuer die Schuelerliste und die Kursliste.
 *
 * @author HLG - 1Inf1 - Jahrgang: 15/16
 * @version 1.0
 */

public class Leser {
    /** Pattern fuer die Kurse */
    private static final Pattern KURSE =
            Pattern.compile("^([a-zäöüß ]+);(\\d+)(!?);(\\d+)(!?);([emz])[^;]*;([wma])[^;]*;([^;]*);([^;]*);([^;]*);([^;]*)",
                    Pattern.CASE_INSENSITIVE);
    /** Pattern fuer die Schueler */
    private static final Pattern SCHUELER =
            Pattern.compile("^([a-zäöüß ]+);([a-zäöüß ]+);(\\d+\\w);(\\w)",
                    Pattern.CASE_INSENSITIVE);

    /**
     * Liest die Kursliste ein.
     *
     * @param stream Gibt die Quelle aus der gelesen werden soll an.
     * @return {@code List<Sportkurs>} mit den eingelesenen Daten.
     * @throws IOException Wenn keine Eingabequelle als Argument angegeben wurde.
     */

    public static List<Sportkurs> leseKurse(InputStream stream) throws IOException {
        if (stream == null) throw new IOException("stream is null");

        BufferedReader br = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));

        return br.lines().parallel()
                .map(KURSE::matcher)
                .filter(Matcher::matches)
                .map(m -> new Sportkurs(m.group(1),
                        Integer.parseInt(m.group(2)),
                        Integer.parseInt(m.group(4)),
                        (m.group(6).equals("e") ?
                                Sportkurs.EINZEL : m.group(6).equals("m") ?
                                Sportkurs.MANNSCHAFT : Sportkurs.ZUSATZ) |
                                (m.group(3).equals("!") ? Sportkurs.ERZWINGE_MIN : 0) |
                                (m.group(5).equals("!") ? Sportkurs.ERZWINGE_KAP : 0),
                        m.group(7).equals("w"),
                        !m.group(7).equals("a"),
                        new boolean[] {
                                !m.group(8).trim().isEmpty(),
                                !m.group(9).trim().isEmpty(),
                                !m.group(10).trim().isEmpty(),
                                !m.group(11).trim().isEmpty()
                        }))
                .collect(Collectors.toList());
    }

    /**
     * Liest die Schuelerlist ein.
     *
     * @param stream Gibt die Quelle aus der gelesen werden soll an.
     * @return {@code Collection<Schueler>} mit den eingelesenen Daten.
     * @throws IOException Wenn keine Eingabequelle als Argument angegeben wurde.
     */

    public static Set<Schueler> leseSchueler(InputStream stream) throws IOException {
        if (stream == null) throw new IOException("Stream is null");

        BufferedReader br = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));

        return br.lines().parallel()
                .map(SCHUELER::matcher)
                .filter(Matcher::matches)
                .map(m -> new Schueler(
                        m.group(1),
                        m.group(2),
                        m.group(3),
                        m.group(4).toLowerCase().startsWith("w")))
                .collect(Collectors.toSet());
    }
}
